import React, { useRef, useState } from "react";
import { useOnClickOutside } from './hooks';
import styled from 'styled-components';
import { Link } from "react-router-dom";
import Logo from './Logo';
import SocialIcon from './SocialIcon';
import { FaCaretDown } from "react-icons/fa";
import Dropdown from './Dropdown';
import Burger from './Burger';
import MenuHomepage from './MenuHomepage';

const Section = styled.section`
width: 100vw;
background-color: ${props => props.theme.text};
`

const NavBar = styled.nav`
display: flex;
justify-content: space-between;
align-items: center;
position: fixed;
top:0;
z-index: 9999;
width: 100%;
height: 160px;
margin: 0 auto;
padding: 20px 50px;
background-color: ${props => props.theme.bg};
box-sizing: border-box;

div.mobile-nav{
    display: none;
}

.hamburger-react {
    top: 21px;
    right: 25px;
    transform: translateY(-50%);
    position: absolute !important;
    display: none;
  }

  @media screen and (max-width: 914px) {
    .hamburger-react {
      display: block;
    }
    div.navigation-menu ul {
        display: none;
      }

      padding: 20px;
      height: 94px;

    h1{
        text-align: center
    }
    div.mobile-nav{
        display: block;
    }
  }
//   @media (max-width: 914px){
//     height: 120px; 
//   }
`

const MenuContainer = styled.ul`
display: flex;
justify-content: space-between;
align-items: center;
list-style: none;
`

const MenuItem = styled.li`
margin: 0 1rem;
color: ${props => props.theme.body};
cursor: pointer;
text-transform: uppercase;
font-family: "Barlow Condensed Bold", sans serif;
display: flex;
align-items: center;
:hover {
    color: ${props => props.theme.btnColor};
}
:hover > svg{
    width: 100%;
    margin-left: 3px;
}
&::after{
    content: ' ';
    display: block;
    width: 0;
    height: 2px;
    background: ${props => props.theme.text};
    transition: width 0.3s ease;
}

&:hover::after{
    width: 100%;
}

> ul {
    -webkit-transition: all 0.3s ease-in;
    -moz-transition: all 0.3s ease-in;
    -o-transition: all 0.3s ease-in;
    -ms-transition: all 0.3s ease-in;
    transition: all 0.3s ease-in;
    height:0; 
    overflow: hidden;
}

:hover > ul {
    display: block;
    height:50px;
}

:nth-child(2){
    width: 128px;
}

> ul li a:hover{
    color: #000;
}

@media (max-width: 915px){
    font-size: 12px;
    :nth-child(2){
        width: 75px;
    }
}

@media (max-width: 1180px) and (min-width: 768px) and (orientation: landscape){
    font-size: 16px;
    :nth-child(2){
        width: 98px;
    }
}

@media (min-width: 1200px){
    font-size: 21px;
}
`

const SubMenuContainer = styled.ul`
position: absolute;
background-color: #fff;
list-style: none;
padding: 0;
top: 89px;
font-size: 1rem;
`

const SubMenuItem = styled.li`
color: ${props => props.theme.text};
padding: 0 10px;
text-align: center;

:first-child{
    border-bottom: 1px solid #000
}

:hover{
    background-color:#85FF10;
}
`

const MenuWrapper = styled.div`

`

const Navigation = () => {
    const [open, setOpen] = useState(false);
    const node = useRef(); 
    useOnClickOutside(node, () => setOpen(false));

const scrollTo = (id) => {

    let element = document.getElementById(id);

    element.scrollIntoView({
        behavior: 'smooth',
        block: 'start',
        inline: 'nearest'
    })
}

    return(
        
        <Section>
            <NavBar>
                <Logo />
                <div className="mobile-nav" ref={node}>
                <Burger open={open} setOpen={setOpen} />
                <MenuHomepage open={open} setOpen={setOpen} scrollTo={scrollTo} />
                </div>
                
                <MenuWrapper className="navigation-menu">
                <MenuContainer>
                    <MenuItem onClick={() => scrollTo('about')} >About</MenuItem>
                    <MenuItem className="submenu-item">Collections <FaCaretDown />
                    <SubMenuContainer>
                        <SubMenuItem onClick={() => scrollTo('madDogs')}>Mad Dogs</SubMenuItem>
                        <SubMenuItem onClick={() => scrollTo('madCars')}>Car Collections</SubMenuItem>
                    </SubMenuContainer>
                    </MenuItem>
                    {/* <MenuItem onClick={() => scrollTo('gallery')} >Gallery</MenuItem> */}
                    <MenuItem onClick={() => scrollTo('roadmap')} >Roadmap</MenuItem>
                    <MenuItem onClick={() => scrollTo('racing')} >Racing</MenuItem>
                    {/* <MenuItem onClick={() => scrollTo('team')} >Team</MenuItem> */}
                    <MenuItem>Team <FaCaretDown />
                    <SubMenuContainer>
                        <SubMenuItem onClick={() => scrollTo('team')}>Meet The Team</SubMenuItem>
                        <SubMenuItem><Link to='/careers'>Careers</Link></SubMenuItem>
                    </SubMenuContainer>
                    </MenuItem>
                    <MenuItem onClick={() => scrollTo('shop')} >Shop</MenuItem>
                    <MenuItem onClick={() => scrollTo('faq')} >Faq</MenuItem>
                    <SocialIcon />
                </MenuContainer>
                </MenuWrapper>
            </NavBar>
        </Section>

    )
}

export default Navigation