import React, { useState, useRef } from "react";
import { useOnClickOutside } from './hooks';
import { HashLink as Link } from 'react-router-hash-link';
import styled from 'styled-components';
// import { Link } from "react-router-dom";
import Logo from './Logo';
import SocialIcon from './SocialIcon';
import {FaCaretDown} from "react-icons/fa";
// import Dropdown from './Dropdown';
import Burger from './Burger';
import Menu from './Menu';

const Section = styled.section`
width: 100vw;
background-color: ${props => props.theme.text};
`

const NavBar = styled.nav`
display: flex;
justify-content: space-between;
align-items: center;
position: fixed;
top:0;
z-index: 9999;
width: 100%;
height: 160px;
margin: 0 auto;
padding: 20px 50px;
background-color: ${props => props.theme.bg};
box-sizing: border-box;

div.mobile-nav{
    display: none;
}

.hamburger-react {
    top: 21px;
    right: 25px;
    transform: translateY(-50%);
    position: absolute !important;
    display: none;
  }

  @media screen and (max-width: 914px) {
    .hamburger-react {
      display: block;
    }
    div.navigation-menu ul {
        display: none;
      }

      padding: 20px;
      height: 94px;

    h1{
        text-align: center
    }
    div.mobile-nav{
        display: block;
    }
  }

//   @media (max-width: 914px){
//     height: 120px; 
//   }
`

const MenuContainer = styled.ul`
display: flex;
justify-content: space-between;
align-items: center;
list-style: none;
`

const MenuItem = styled.li`
margin: 0 1rem;
color: ${props => props.theme.body};
cursor: pointer;
text-transform: uppercase;
font-family: "Barlow Condensed Bold", sans serif;
display: flex;
align-items: center;
:hover, > a:hover {
    color: ${props => props.theme.btnColor};
}
&::after{
    content: ' ';
    display: block;
    width: 0;
    height: 2px;
    background: ${props => props.theme.text};
    transition: width 0.3s ease;
}

&:hover::after{
    width: 100%;
}

> ul {
    -webkit-transition: all 0.3s ease-in;
    -moz-transition: all 0.3s ease-in;
    -o-transition: all 0.3s ease-in;
    -ms-transition: all 0.3s ease-in;
    transition: all 0.3s ease-in;
    height:0; 
    overflow: hidden;
}

:hover > ul {
    display: block;
    height:50px;
}

:nth-child(2){
    width: 128px;
}

> ul li a:hover{
    color: #000;
}

@media (max-width: 914px){
    font-size: 12px;
    :nth-child(2){
        width: 75px;
    }
}

@media (max-width: 1180px) and (min-width: 768px) and (orientation: landscape){
    font-size: 16px;
    :nth-child(2){
        width: 98px;
    }
}

@media (min-width: 1200px){
    font-size: 21px;
}
`

const SubMenuContainer = styled.ul`
position: absolute;
background-color: #fff;
list-style: none;
padding: 0;
top: 89px;
font-size: 1rem;
`

const SubMenuItem = styled.li`
color: ${props => props.theme.text};
padding: 0 10px;
text-align: center;
:first-child{
    border-bottom: 1px solid #000
}

:hover{
    background-color:#85FF10;
}
`

const MenuWrapper = styled.div`

`


const Navigation2 = () => {
    // const [dropdown, setDropdown] = useState(false);
    const [open, setOpen] = useState(false);
    const node = useRef(); 
    useOnClickOutside(node, () => setOpen(false));
    return(
        
        <Section>
            <NavBar>
                <Logo />
                <div className="mobile-nav" ref={node}>
                <Burger open={open} setOpen={setOpen} />
                <Menu open={open} setOpen={setOpen} />
                </div>
                
                <MenuWrapper className="navigation-menu">
                <MenuContainer>
                    <MenuItem><Link to='/#about'>About</Link></MenuItem>
                    <MenuItem>Collections <FaCaretDown />
                    <SubMenuContainer>
                        <SubMenuItem><Link to='/#madDogs'>Mad Dogs</Link></SubMenuItem>
                        <SubMenuItem><Link to='/#madCars'>Car Collections</Link></SubMenuItem>
                    </SubMenuContainer>
                    </MenuItem>
                    {/* <MenuItem><Link to='/#gallery'>Gallery</Link></MenuItem> */}
                    <MenuItem><Link to='/#roadmap'>Roadmap</Link></MenuItem>
                    <MenuItem><Link to='/#racing'>Racing</Link></MenuItem>
                    {/* <MenuItem><Link to='/#team'>Team</Link></MenuItem> */}
                    <MenuItem>Team <FaCaretDown />
                    <SubMenuContainer>
                        <SubMenuItem><Link to='/#team'>Meet The Team</Link></SubMenuItem>
                        <SubMenuItem><Link to='/careers'>Careers</Link></SubMenuItem>
                    </SubMenuContainer>
                    </MenuItem>
                    <MenuItem><Link to='/#shop'>Shop</Link></MenuItem>
                    <MenuItem><Link to='/#faq'>Faq</Link></MenuItem>
                    <SocialIcon />
                </MenuContainer>
                </MenuWrapper>
            </NavBar>
        </Section>

    )
}

export default Navigation2