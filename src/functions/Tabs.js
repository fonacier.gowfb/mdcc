function Tabs({filterCategory, tabsData}){
    return(
        <ul className="text-left">
 {
 tabsData.map((category, index)=>{
      return (
            <li key={index}>
                <a className="text-uppercase" onClick={()=> filterCategory(category)} >{category}</a>
            </li>
        
      )
 })
 }


</ul>
    )
}

export default Tabs;