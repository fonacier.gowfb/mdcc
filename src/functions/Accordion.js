import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Minus } from '../Icons/Minus'
import { Plus } from '../Icons/Plus'



const Container = styled.div`
cursor: pointer;
padding: 1rem 0.5rem;
display: flex;
flex-direction: column;
border-bottom: 1px solid ${props => props.theme.carouselColor};

:first-child{
    border-top: 1px solid ${props => props.theme.carouselColor};
}
`
const Box = styled.div`

`

const Title = styled.div`
color: ${(props) => props.theme.body};
font-size: ${props => props.theme.fontxxl};
font-family: "Barlow Condensed Bold", sans-serif;
text-transform: uppercase;
font-weight: 600;
display: flex;
justify-content: start;
align-items: center;

@media (max-width: 428px){
    font-size: ${props => props.theme.fontlx};
    line-height: 2rem;
}
`
const Reveal = styled.div`
display: ${props => props.clicked ? 'inline-block' : 'none'};
margin-top: 1rem;
margin-left: 60px;
color: ${(props) => props.theme.body};
font-size: ${props => props.theme.fontxl};
font-weight: 300;
line-height: 3rem;

@media (max-width: 428px){
    font-size: 1.25rem;
    line-height: 1.25rem;
}
`

const Name = styled.div`
display: flex;
align-items: center;
margin-left: 30px;
`
const Indicator = styled.span`
font-size: ${props => props.theme.fontxxl};

display: flex;
justify-content: center;
align-items: center;

svg{
    width: 2rem;
    height: auto;
    fill: ${props => props.theme.carouselColor};
}

@media (max-width: 48em){
    font-size: ${props => props.theme.fontxl};
}

@media (max-width: 428px){
    svg{
        width: 1.25rem;
    }
}
`

const Accordion = ({title, children, ScrollTrigger}) => {
    const [collapse, setCollapse] = useState(false);

    useEffect(() => {
        ScrollTrigger.refresh()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [collapse])
    
  return (
    <Container>
        <Box>
            <Title onClick={() => setCollapse(!collapse)}>
                {
                    collapse ? 
                    <Indicator>
                        <Minus />
                    </Indicator> : <Indicator>
                        <Plus />
                    </Indicator>
                }
                <Name>
                    <span>{title}</span>
                </Name>
            </Title>
        </Box>
        <Box>
            <Reveal clicked={collapse}>
                {children}
            </Reveal>
        </Box>
    </Container>
  )
}

export default Accordion