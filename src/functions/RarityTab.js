import React, { useEffect, useState } from 'react'
import styled from 'styled-components'
import { Filter } from '../Icons/Filter'



const Container = styled.div`
cursor: pointer;
padding: 1rem 0 0;
display: flex;
flex-direction: column;
border-bottom: 2px solid ${props => props.theme.text};
margin: 0 0 3rem;

@media (max-width: 48em){
    margin: 2rem 0;

}
`
const Title = styled.div`
color: ${(props) => props.theme.text};
font-size: ${props => props.theme.fontlg};
font-weight: 600;
display: flex;
justify-content: space-between;
align-items: center;
margin-bottom: 1rem;
`
const Reveal = styled.div`
display: ${props => props.clicked ? 'inline-block' : 'none'};
color: ${props => props.theme.text};
font-size: ${props => props.theme.fontlg};
font-weight: 300;
line-height: 3rem;
`

const Name = styled.div`
display: flex;
align-items: center;

div:first-child{
    width: 20px;
    height: auto;
    display: block;
}
`
const Indicator = styled.span`
font-size: ${props => props.theme.fontxxl};

display: flex;
justify-content: center;
align-items: center;

svg{
    width: 2rem;
    height: auto;
    fill: ${props => props.theme.carouselColor};
}

@media (max-width: 48em){
    font-size: ${props => props.theme.fontxl};


}
`

const RarityTab = ({title, children, ScrollTrigger}) => {
    const [collapse, setCollapse] = useState(false);

    useEffect(() => {
        ScrollTrigger.refresh()
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [collapse])
    
  return (
    <Container>
        <Title onClick={() => setCollapse(!collapse)}>
            <Name>
                <div><Filter /></div>
                <div>{title}</div>
            </Name>
            {
                collapse ? 
                <Indicator>
                    {/* <Minus /> */}
                </Indicator> : <Indicator>
                    {/* <Plus /> */}
                </Indicator>
            }
        </Title>
        <Reveal clicked={collapse}>
            {children}
        </Reveal>
    </Container>
  )
}

export default RarityTab