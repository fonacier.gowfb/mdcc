import styled from 'styled-components';

export const StyledMenu = styled.nav`
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: #000;
  height: 100vh;
  text-align: left;
  padding: 2rem;
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.3s ease-in-out;

  transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(-100%)'};
  
  @media (max-width: 428px) {
    width: 100%;
    .menu-mobile-socialicons{
        justify-content: space-evenly;
    }

    .mobile_menu{
        max-height: 120px;
    }

  li > ul {
      -webkit-transition: all 0.3s ease-in;
      -moz-transition: all 0.3s ease-in;
      -o-transition: all 0.3s ease-in;
      -ms-transition: all 0.3s ease-in;
      transition: all 0.3s ease-in;
      height:0; 
      width: 70%;
      margin: 30px 30px 0;
      overflow: hidden;
  }

  li > ul li {
    color: #000;
    font-family: "Barlow Condensed Bold", sans-serif;
    letter-spacing: normal;
  }
  
  li:hover > ul {
      display: block;
      height:auto;
  }
  }

  @media (max-width: 914px) and (min-width: 429px) {
    width: 100%;
    .menu-mobile-socialicons{
        justify-content: center;
    }

    .mobile_menu{
        max-height: 70px;
    }

  li > ul {
      -webkit-transition: all 0.3s ease-in;
      -moz-transition: all 0.3s ease-in;
      -o-transition: all 0.3s ease-in;
      -ms-transition: all 0.3s ease-in;
      transition: all 0.3s ease-in;
      height:0; 
      width: 150px;
      margin-top: 36px !important;
      margin: 0 auto;
      overflow: hidden;
  }

  li > ul li {
    color: #000;
    font-family: "Barlow Condensed Bold", sans-serif;
    font-size: 1.25em;
    padding: 0.5rem 0;
    letter-spacing: normal;
  }
  
  li:hover > ul {
      display: block;
      height:auto;
      width: 230px;
  }

  li > ul li:hover {
    background: #85FF10;
}

  h1.submenu-logo a > img{
    max-height: 70px;
  }
  }

  li {
    font-size: 2rem;
    font-family: "Barlow Condensed Bold", sans serif;
    text-transform: uppercase;
    padding: 1rem 0;
    font-weight: bold;
    letter-spacing: 0.1rem;
    color: ${props => props.theme.body};
    text-decoration: none;
    transition: color 0.3s linear;
    display: flex;
    justify-content: center;
    
    @media (max-width: 427px) {
      font-size: 1rem;
      padding: 0.5rem 0;
      text-align: center;
    }

    @media (max-width: 914px) and (min-width: 429px) and (orientation: landscape) {
      font-size: 0.75rem;
      text-align: center;
      padding: 0.25rem 0;
    }

    &:hover {
      color: #85FF10;
    }
  }

  li {
    list-style: none;
  }
`;