import React from "react";
import styled from 'styled-components';

const Btn = styled.a`
display: inline-block;
background-color: ${props => props.theme.btnColor};
color: ${props => props.theme.body};
outline: none;
border: none;

font-size: ${props => props.theme.fontlg};
font-family: "Barlow Condensed Bold", sans serif;
padding: 0.9rem 2.3rem;
border-radius: ${props => props.theme.btnRadius};
cursor: pointer;
transition: all 0.2s ease;
position: relative;
&:hover{
    transform: scale(0.9);
    color: ${props => props.theme.body};

}

&::after{
    content: ' ';
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) scale(0);
    border: 2px solid ${props => props.theme.btnColor};
    width: 100%;
    height: 100%;
    border-radius: ${props => props.theme.btnRadius};
    transition: all 0.2s ease;
}

&:hover::after{
    transform: translate(-50%, -50%) scale(1);
    padding: 0.3rem;
}

a{
    display: flex;
    justify-content: center;
}

@media screen and (max-width: 428px) {
    font-size: ${props => props.theme.fontmd};
    padding: 0.9rem 1.15rem;
}
`

const Button = ({text, link}) => {
    return(
        <Btn href={link} aria-label={text} rel="noreferrer">
            {text}
        </Btn>
    )
}

export default Button