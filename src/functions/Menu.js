import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';
import { HashLink as Link } from 'react-router-hash-link';
import { StyledMenu } from './Menu.styled';
import Logo from './Logo';
import SocialIcon from './SocialIcon';
import { FaCaretDown } from "react-icons/fa";

const SubMenuContainer = styled.ul`
position: absolute;
background-color: #fff;
list-style: none;
padding: 0;
font-size: 1rem;
`

const SubMenuItem = styled.li`
a{
color: ${props => props.theme.text};
padding: 0 10px;
text-align: center;
}

:first-child{
    border-bottom: 1px solid #000
}
`

const MenuWrapper = styled.div`
display: block;
margin-bottom: 20px;

li{
  display: flex;
    align-items: center;
}

@media (max-width: 914px) and (min-width: 429px){
  display: block;
  justify-content: center;
  width: 100%;

  &>li a, li{
    font-size: 1.5rem;
  }
}

@media (max-width: 914px) and (min-width: 429px) and (orientation: landscape){
  &>li a, li{
    font-size: 0.75rem;
  }

  display: flex;
  padding: 0;
  li{
    padding: 5px;
    font-size: 0.75rem;
    letter-spacing: 0.25em;
  }

  a{
    letter-spacing: 0.25em;
    padding: 0;
  }
}
`

// const MenuDiv = styled.div`
// @media (max-width: 914px) and (min-width: 429px){
//   padding:0;
//   display: flex;

//   > li{
//     padding: 50px 10px;
//   }
// }
// `

const Menu = ({ open, setOpen }) => {
  return (
    <StyledMenu open={open}>
       <Logo />
       <MenuWrapper className='menuWrapper'>
            <li><Link to='/#about'>About</Link></li>
            <li>Collections<FaCaretDown />
            <SubMenuContainer>
                <SubMenuItem><Link to='/#madDogs'>Mad Dogs</Link></SubMenuItem>
                <SubMenuItem><Link to='/#madCars'>Car Collections</Link></SubMenuItem>
            </SubMenuContainer>
            </li>
            <li><Link to='/#roadmap'>Roadmap</Link></li>
            <li><Link to='/#racing'>Racing</Link></li>
            {/* <li><Link to='/#team'>Team</Link></li> */}
            <li>Team<FaCaretDown />
            <SubMenuContainer>
                <SubMenuItem><Link to='/#team'>Meet The Team</Link></SubMenuItem>
                <SubMenuItem><Link to='/careers' onClick={() => {setOpen(!open);}}>Careers</Link></SubMenuItem>
            </SubMenuContainer>
            </li>
            <li><Link to='/#shop'>Shop</Link></li>
            <li><Link to='/#faq'>Faq</Link></li>
       </MenuWrapper>
      <SocialIcon />
    </StyledMenu>
  )
}
Menu.propTypes = {
  open: bool.isRequired,
};
export default Menu