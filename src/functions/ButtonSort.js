import React from "react";
import styled from 'styled-components';

const Btn = styled.a`
display: inline-block;
background-color: transparent;
color: ${props => props.theme.text};
outline: none;
border: none;

font-size: ${props => props.theme.fontlg};
padding: 1rem 4rem;
font-weight: 900;
cursor: pointer;
position: relative;

`

const Button = ({text, link}) => {
    return(
            <Btn href={link} aria-label={text} target="_blank" rel="noreferrer">
            {text}
            </Btn>
    )
}

export default Button