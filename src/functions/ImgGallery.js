import React from "react";

const ImgGallery = ({link, text}) => {
    return(
        <img src={link} alt={text} />
    )
}

export default ImgGallery