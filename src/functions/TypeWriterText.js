import React from "react";
import styled from 'styled-components';
// import Typewriter from 'typewriter-effect';
import Button from "./Button";

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
width: 80%;
color: ${props => props.theme.body};
align-self: flex-start;
font-family: "Floyd", sans-serif;
span{
    text-transform: uppercase;
    font-family: 'Akaya Telivigala', cursive;
}

.text-1{
    color: blue;
}

.text-2 {
    color: orange;
}

.text-3{
    color: red;
}
`

const SubTitle = styled.h3`
font-size: ${props => props.theme.fontxl};
text-transform: uppercase;
color: ${props => props.theme.body};
font-weight: 600;
margin-bottom: 1rem;
width: 80%;
align-self: flex-start;
`
const ButtonContainer = styled.div`
width: 80%;
align-self: flex-start;
`

const TypeWriterText = () => {
    return(
        <>
        <Title>
            Mad Dog Car Club
            {/* <Typewriter
            options={{
                autoStart: true,
                loop: true,
            }}
            onInit={(typewriter) => {
            typewriter.typeString("<span className='text-1'>NFT.</span>")
                .pauseFor(2000)
                .deleteAll()
                .typeString("<span className='text-2'>Collectible Items.</span>")
                .pauseFor(2000)
                .deleteAll()
                .typeString("<span className='text-3'>Ape Killers!</span>")
                .pauseFor(2000)
                .deleteAll()
                .start()
            }}
            /> */}

        </Title>
        <SubTitle>All About The Drive</SubTitle>
        {/* <ButtonContainer><Button text="Explore" link="#about" /></ButtonContainer> */}
        
        </>
    )
}

export default TypeWriterText