import React from "react";
import styled from 'styled-components';
import Vector from '../Icons/Vector';
import Vector2 from "../Icons/Vector2";

const VectorContainer = styled.div`
position: absolute;
top: 0.5rem;
left: 50%;
transform: translateX(-50%);
width: 100%;
height: 100%;
overflow: hidden;

svg{
    width: 100%;
    height: 100%;
}

.mobile-svg{
    display: none;
}

.desktop-svg{
    display: flex;
}

@media (max-width: 428px){
    left: 90%;   
    height: 93%;
    top: 225px;

    .mobile-svg{
        display: flex;
    }
    
    .desktop-svg{
        display: none;
    }
}

@media (max-width: 914px) and (min-width: 429px){
    // left: 90%;   
    // height: 90%;
    // top: 225px;

    .mobile-svg{
        display: flex;
    }
    
    .desktop-svg{
        display: none;
    }
}
`

const DrawSvg = () => {
    

    return(
        <>
        <VectorContainer>
            <Vector className="desktop-svg" />
            <Vector2 className="mobile-svg" />
        </VectorContainer>
        </>
    )
}

export default DrawSvg