import React from 'react';
import styled from 'styled-components';
import { bool } from 'prop-types';
import { StyledMenu } from './Menu.styled';
import Logo from './Logo';
import SocialIcon from './SocialIcon';
import { FaCaretDown } from "react-icons/fa";
import { Link } from 'react-router-dom';

const SubMenuContainer = styled.ul`
position: absolute;
background-color: #fff;
list-style: none;
padding: 0px;
font-size: 1rem;
`

const SubMenuItem = styled.li`
color: ${props => props.theme.text};
padding: 0 10px;
text-align: center;

:first-child{
    border-bottom: 1px solid #000
}
`

const MenuWrapper = styled.div`
display: block;
margin-bottom: 20px;

li{
  display: flex;
    align-items: center;
}

@media (max-width: 914px) and (min-width: 429px){
  display: block;
  justify-content: center;
  width: 100%;

  &>li{
    font-size: 1.5rem;
  }
}

@media (max-width: 914px) and (min-width: 429px) and (orientation: landscape){
  display: flex;
  padding: 0;
  li{
    padding: 5px;
    font-size: 0.75rem;
    letter-spacing: 0.25em;
  }
}

`

// const MenuDiv = styled.div`
// @media (max-width: 914px) and (min-width: 429px){
//   padding: 0;
//   display: flex;

//   > li{
//     padding: 50px 10px;
//   }
// }
// `

const MenuHomepage = ({ scrollTo, open, setOpen}) => {
  return (
    <StyledMenu open={open}>
       <Logo />
       <MenuWrapper className='menuWrapper'>
          <li onClick={() => {scrollTo('about'); setOpen(!open);}}>About</li>
          <li>Collections<FaCaretDown />
          <SubMenuContainer>
              <SubMenuItem onClick={() => {scrollTo('madDogs'); setOpen(!open);}}>Mad Dogs</SubMenuItem>
              <SubMenuItem onClick={() => {scrollTo('madCars'); setOpen(!open);}}>Car Collections</SubMenuItem>
          </SubMenuContainer>
          </li>
          <li onClick={() => {scrollTo('roadmap'); setOpen(!open);}}>Roadmap</li>
          <li onClick={() => {scrollTo('racing'); setOpen(!open);}}>Racing</li>         
          {/* <li onClick={() => {scrollTo('team'); setOpen(!open);}}>Team</li>            */}
          <li>Team<FaCaretDown />
          <SubMenuContainer>
              <SubMenuItem onClick={() => {scrollTo('team'); setOpen(!open);}}>Meet The Team</SubMenuItem>
              <SubMenuItem><Link to='/careers'>Careers</Link></SubMenuItem>
          </SubMenuContainer>
          </li>
          <li onClick={() => {scrollTo('shop'); setOpen(!open);}}>Shop</li>
          <li onClick={() => {scrollTo('faq'); setOpen(!open);}}>Faq</li>
       </MenuWrapper>
      <SocialIcon />
    </StyledMenu>
  )
}
MenuHomepage.propTypes = {
  open: bool.isRequired,
};
export default MenuHomepage