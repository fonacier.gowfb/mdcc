import React from "react";
import { Link } from "react-router-dom";
import styled from 'styled-components';
import LogoMdcc from '../assets/mdcc_logo.png';

const LogoText = styled.h1`
font-family: 'Akaya Telivigala', cursive;
font-size: ${props => props.theme.fontxl};
color: ${props => props.theme.text};
transition: all 0.2s ease;
padding-bottom: 10px;
&:hover{
    transform: scale(1.1);
}

img{
    width: auto;
    transition: 0.4s;
    // max-width: 100%;
    max-height: 150px;
}

@media (max-width:428px){
    img{
        max-height: 78px;
    }
}

@media (max-width: 914px){
    img{
        max-height: 80px;
    }
}
`

const Logo = () => {
    return(
        <LogoText>
            <Link to="/">
                <img src={LogoMdcc} alt="MDCC" className="mobile_menu" />
            </Link>
        </LogoText>
    )
}
export default Logo