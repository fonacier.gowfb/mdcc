import React from "react";
import styled from 'styled-components';
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/effect-cards";
import "swiper/css/pagination";
import "swiper/css/navigation";

import { Pagination, Navigation, Autoplay, EffectCoverflow } from "swiper";

import img1 from '../assets/Nfts/md1.jpg';
import img2 from '../assets/Nfts/md2.jpg';
import img3 from '../assets/Nfts/md3.jpg';
import img4 from '../assets/Nfts/md4.jpg';
import img5 from '../assets/Nfts/md5.jpg';
import img6 from '../assets/Nfts/md6.jpg';
import img7 from '../assets/Nfts/md7.jpg';
import img8 from '../assets/Nfts/md8.jpg';
import img9 from '../assets/Nfts/md9.jpg';
import img10 from '../assets/Nfts/md10.jpg';

const Container = styled.div`
width: 100%;
height: 100%;

.swiper{
    overflow: hidden;
}


img{
    width:300px;
}

.swiper-button-next{
    color: ${props => props.theme.body};
    right: 50px;
    top: 60%;
    width: 4rem;
}

.swiper-button-prev{
    color: ${props => props.theme.body};
    left: 50px;
    top: 60%;
    width: 4rem;
}

@media (max-width:280px){
    img{
        width:90px !important;
    }
} 

@media (max-width:428px){
    img{
        width:130px;
    }

    .swiper-button-next{
        color: ${props => props.theme.body};
        right: 30px;
        top: 60%;
        width: 1rem;
    }

    .swiper-button-next:after{
        font-size: 1.25rem;
    }
    
    .swiper-button-prev{
        color: ${props => props.theme.body};
        left: 33px;
        top: 60%;
        width: 1rem;
    }

    .swiper-button-prev:after{
        font-size: 1.25rem;
    }
}

@media (max-width: 914px) and (min-width:429px){
    img{
        width:250px;
    }

    .swiper-button-next{
        color: ${props => props.theme.body};
        right: 60px;
        top: 60%;
        width: 1rem;
    }
    
    .swiper-button-prev{
        color: ${props => props.theme.body};
        left: 63px;
        top: 60%;
        width: 1rem;
    }
}
`

const Carousel = () => {
    return(
        <Container>
            <Swiper

                loop={true}
                speed={1000}
                autoplay={{
                    delay: 3000,
                }}
                effect={'coverflow'}
                grabCursor={true}
                centeredSlides={true}
                slidesPerView={3}
                spaceBetween={10}
                navigation={{
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }}
                coverflowEffect={{
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: false,
                }}
                modules={[EffectCoverflow, Pagination, Navigation, Autoplay]}
                className="mySwiper"
            >
                <SwiperSlide> <img src={img1} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img2} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img3} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img4} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img5} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img6} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img7} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img8} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img9} alt="MDCC" /> </SwiperSlide>
                <SwiperSlide> <img src={img10} alt="MDCC" /> </SwiperSlide>
                {/* <!-- If we need navigation buttons --> */}
                <div className="swiper-button-prev"></div>
                <div className="swiper-button-next"></div>
            </Swiper>
        </Container>
    )
}

export default Carousel