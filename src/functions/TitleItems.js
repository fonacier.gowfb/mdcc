function TitleItems({title}){
    
    return (
        <>
              
         {  
          title.map((value, index)=>{
            const {category} = value;
            return (
            <p className="First-text" key={index}>{category}</p>
            
           )
          })
         
          
        }
        </>
        
    )
}

export default TitleItems;