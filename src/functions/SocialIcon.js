import React from "react";
import styled from 'styled-components';
import twiiter from "../assets/img/social-icons/Twitter_Logo.png";
import discord from "../assets/img/social-icons/Discord-Logo.png";
import instragram from "../assets/img/social-icons/Instagram_Logo.png";

const SocialContainer = styled.div`
display: flex;
justify-content: space-between;
align-items: center;

@media (max-width: 914px) and (min-width: 429px){
    width: 50%;
    margin: 0 auto;
    justify-content: center;
}
`

const SocialBtn = styled.a`
padding: 0 10px;
img{
    width:50px;
    height:auto;
}

@media (min-width: 914px){
    img{
        width:40px;
    }
}

@media (max-width: 915px) and (min-width: 429px){
    img{
        width:30px;
    }
}

@media (max-width: 914px) and (min-width: 429px) and (orientation: landscape){
    img{
        width:50px;
    }
}
`

const SocialIcon = ({link}) => {
    return(
        <SocialContainer className="menu-mobile-socialicons">
            <SocialBtn href='https://twitter.com/MadDogCarClub' >
                <img src={twiiter} alt="twitter"/>
            </SocialBtn>
            <SocialBtn href='https://www.instagram.com/maddogcarclub/' >
                <img src={instragram} alt="instagram"/>
            </SocialBtn>
            <SocialBtn href='https://discord.com/invite/maddogcarclub' >
                <img src={discord} alt="discord"/>
            </SocialBtn>
        </SocialContainer>
    )
}
export default SocialIcon