import { ThemeProvider } from 'styled-components';
import GlobalStyles from './styles/GlobalStyles';
import {light} from './styles/Themes';
import "./App.css";
import styled from 'styled-components';

import "./fonts/Floyd.ttf";
import "./fonts/Barlow_Condensed/BarlowCondensed-Medium.ttf";
import "./fonts/Barlow_Condensed/BarlowCondensed-Bold.ttf";
import "@fontsource/barlow";

import Navigation from './functions/Navigation';
import Home from './components/sections/Home';
import About from './components/sections/About';
import About2 from './components/sections/About2';
import Rarity from './components/sections/Rarity';
import MadCar from './components/sections/Madcar';
import Roadmap from './components/sections/Roadmap';
import MadCar01 from './components/sections/Madcar01';
import Team from './components/sections/Team';
import Shop from './components/sections/Shop';
import Faq from './components/sections/Faq';
import ScrollToTop from './functions/ScrollToTop';

const HideMobile = styled.div`
@media (max-width:428px){
display: none;
}
`

function App() {
  return (
    <>
    <GlobalStyles />
     <ThemeProvider theme={light}>
       <Navigation />
       <Home />
       <About />
       <About2 />
       <Rarity />       
       <MadCar />  
       <Roadmap />
       <MadCar01 />
       <Team />
       <Shop />
       <Faq />
       <ScrollToTop />
     </ThemeProvider>
    </>
  );
}

export default App;
