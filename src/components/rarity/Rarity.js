import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import React, { useState, useLayoutEffect, useRef, useEffect } from 'react';
import styled from "styled-components";
import { ThemeProvider } from 'styled-components';
import GlobalStyles from '../../styles/GlobalStyles';
import "../../App.css";

import "../../fonts/Floyd.ttf";
import {light} from '../../styles/Themes';
import FooterBackground from '../../assets/img/Background.jpg';
import Navigation from "../../functions/Navigation2";
import SocialIcon from "../../functions/SocialIcon";
import { NftGallery } from 'react-nft-gallery';
import ButtonSort from '../../functions/ButtonSort';
import ImgGallery from "../../functions/ImgGallery";

import Img1 from "../../assets/Nfts/md1.jpg";
import Tabs from "../../functions/Tabs";
import Items from "../../functions/Items";
import Data from "../../functions/Data"
import RarityTab from '../../functions/RarityTab';
import TitleItems from '../../functions/TitleItems';
import ScrollToTop from '../../functions/ScrollToTop';
import { Link } from 'react-router-dom';

const Section = styled.section`
min-height: 100vh;
width: 100%;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;
`

const FooterSection = styled.section`
width: 100%;
padding: 50px 0;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;
background-image: url(${FooterBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: start;
margin-top: 180px;
margin-bottom: 50px;

@media (max-width: 428px){
    width: 100%;
    display: block;
    padding: 0 20px;
    margin-top: 80px;
}
`

const Box = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

const BoxSort = styled.div`
width: 20%;
height: 100%;
min-height: 500px;
margin-top: 100px;
display: flex;
flex-direction: column;
justify-content: start;
align-items: center;


ul{
    list-style: none;
    padding-left: 0;
    padding-bottom: 0;
}

li {
    padding: 0 20px;
    border-bottom: 2px solid #000;
    font-weight: 600;
}

li:first-child{
    border-top: 2px solid #000;
}

li:last-child{
    border-bottom: none;
}

li a{
    cursor:pointer;
}

li a:hover{
    color:${props => props.theme.btnColor};
}


div.tabs_desktop{
    display: block !important;
}

@media (max-width:428px){
    min-height: 0;
    margin-top: 50px;

    div.tabs_desktop{
        display: none !important;
    }
}
`

const BoxGallery = styled.div`
width: 80%;
height: 100%;
display: block;

ul{
    list-style: none;
    padding-left: 0;
    padding-bottom: 0;
}

li {
    padding: 0 20px;
    border-bottom: 2px solid #000;
    font-weight: 600;
}

li:first-child{
    border-top: 2px solid #000;
}

li:last-child{
    border-bottom: none;
}

li a{
    cursor:pointer;
}

li a:hover{
    color:${props => props.theme.btnColor};
}

div.tabs_mobile{
    display: none !important;
}

@media (max-width:428px){
    
    div.tabs_mobile{
        width: 150px;
        display: block !important;
        margin-bottom: 50px;
    }

    width: 100%;
}
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
font-family: "Floyd", sans-serif;
text-transform: uppercase;
color: ${props => props.theme.text};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: left;
margin-bottom: 50px;
`

const FooterContainer = styled.div`
width: 75%;
min-height: 20vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;
`

const FooterBox = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

div{
    margin-top: 20px;
}

div a img{
    width: 35px;
}
`

const FooterTitle = styled.p`
font-size: ${props => props.theme.fontmd};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: center;

a:hover{
    color:#85FF10;
  }
`

const BtnContainer = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
align-content: start;
width:100%;
padding-right: 10px;

div > div ul{
    margin-bottom: 0;
}
`

const GalleryWrapper = styled.div`

`

const CardItems = styled.div`
div div > div.card{
    background-color: transparent !important;
    background-clip: border-box;
    border: none;
    border-radius: 0;
}

div div > div.card h3 {
    font-size: ${props => props.theme.fontlg};
    font-family: "Barlow Condensed Bold", sans serif;
    text-transform: uppercase;
}

p.First-text{    
    font-size: ${props => props.theme.fontxl};
    font-family: "Barlow Condensed Medium", sans serif;
    display: none;
}

p.First-text:nth-child(1){
    display: inline-block;
}
@media (max-width:428px){
    .row>* {
        width: 50%;
    }
}
`

const CardContent = styled.div`
width: 200px;
box-sizing: border-box;
float: left;
display: block;
margin: 20px 10px;

img {
    width:200px;
}
`

const Content = styled.p`

`

const Gallery = () => {
    const [data, setData] = useState(Data);

    const categoryData = Data.map((value)=>{
          return value.category
     });

    const tabsData= [...new Set(categoryData)];
    
    const filterCategory=(category) =>{
        
       const filteredData =  Data.filter((value)=>{
           
           return value.category === category;
       })
       
       setData(filteredData);
       
    }

    const ref = useRef(null);
    gsap.registerPlugin(ScrollTrigger);
    useLayoutEffect(() => {
    
    let element = ref.current;

    ScrollTrigger.create({
        trigger: element,
        start:'top center',
        end:'top center',
        pin:true,   
        pinSpacing:false, 
        scrub:1,
        // markers:true,
    })

    return () => {
        ScrollTrigger.kill();
    };
    }, [])

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    return(
        <>
        <GlobalStyles />
        <ThemeProvider theme={light}>
            <Navigation />
            <Section id='home'>
                <Container>
                    <BoxSort>
                    <BtnContainer className="tabs_desktop"  ref={ref}>
                        <RarityTab ScrollTrigger={ScrollTrigger} title="FILTER">
                            <Tabs filterCategory={filterCategory} tabsData={tabsData}/>
                        </RarityTab>
                       
                    </BtnContainer>
                    </BoxSort>
                    <BoxGallery>
                        <GalleryWrapper>
                        <Title>Mad Dog Rarity Chart</Title>
                        <BtnContainer className="tabs_mobile"  ref={ref}>
                        <RarityTab ScrollTrigger={ScrollTrigger} title="FILTER">
                            <Tabs filterCategory={filterCategory} tabsData={tabsData}/>
                        </RarityTab>
                        </BtnContainer>
                            <CardItems>
                            <TitleItems title={data}/>
                            <Items data={data} />
                                
                            </CardItems>
                        </GalleryWrapper>

                    {/* <NftGallery
                        ownerAddress="vitalik.eth"
                        apiUrl="https://rocky-ocean-73265.herokuapp.com"
                        isProxyApi={true}
                    /> */}
                    </BoxGallery>
                </Container>
            </Section>  
            <FooterSection>
            <ScrollToTop />
                <FooterContainer>
                    <Box>
                        <FooterBox>
                            <FooterTitle>
                            © MAD DOG CAR CLUB<br/>
                            ALL RIGHTS RESERVED<br/>
                            <Link to='/terms'>TERMS</Link> &#38; <Link to='/policy'>POLICY</Link>  
                            </FooterTitle>
                            <SocialIcon />
                        </FooterBox>
                    </Box>
                </FooterContainer>
            </FooterSection>
        </ThemeProvider>      
        </>
    )
}

export default Gallery