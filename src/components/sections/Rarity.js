import React from "react";
import styled from 'styled-components';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import Carousel from "../../functions/Carousel";
import RarityBackground from '../../assets/img/Gallery.jpg';
import TearPaperBackground from '../../assets/img/Paper_Tear.png';
import Button from "../../functions/Button";
import { Link } from "react-router-dom";

const Section = styled.section`
min-height: 126vh;
width: 100%;
background-color: ${props => props.theme.text};
background-image: url(${RarityBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 54%;
display: flex;
justify-content: center;
align-items: flex-start;
position: relative;

@media (max-width:428px){
    min-height: 100%;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
    padding: 0;
}
`

const Container = styled.div`
width: 75%;
height: 100%;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

@media (max-width: 914px){
    width: 100%;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxxl};
text-transform: uppercase;
width: 100%;
color: ${props => props.theme.body};
align-self: flex-start;
font-family: "Floyd", sans-serif;
span{
    text-transform: uppercase;
    font-family: 'Akaya Telivigala', cursive;
}

text-align: center;
margin-bottom: 20px;

@media (max-width:428px){
    font-size: ${props => props.theme.fontxxl};
}
`

const DateTitle = styled.h2`
font-size: ${props => props.theme.fontlx};
text-transform: uppercase;
width: 100%;
color: ${props => props.theme.btnColor};
align-self: flex-start;
font-family: "Floyd", sans-serif;
span{
    text-transform: uppercase;
    font-family: 'Akaya Telivigala', cursive;
}

text-align: center;
margin-bottom: 20px;

@media (max-width:428px){
    font-size: ${props => props.theme.fontsm};
    margin-bottom: 5px;
}
`

const Subtext = styled.h3`
font-family: "Barlow Condensed Medium", sans serif;
font-size: ${props => props.theme.fontlg};
text-transform: uppercase;
color: ${props => props.theme.text};
margin-bottom: 1rem;
width: 80%;
align-self: center;
text-align: center;

@media (max-width:428px){
    font-size: ${props => props.theme.fontsm};
    margin-bottom: 5px;
}
`

const SubContainer = styled.div`
width: 100%;
height: 100%;
margin-top: -73px;
margin-bottom: 50px;
padding: 0 10px;
display: flex;
justify-content: center;
align-items: center;

@media (max-width:428px){
    padding: 0;
    margin-top: -28px;
}
`

const Box = styled.div`
width:30%;
margin: 0 40px;
padding: 20px 0 0;
min-height: 170px;
background-image: url(${TearPaperBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 54%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

@media (max-width:428px){
    width:95%;
    font-size: ${props => props.theme.fontsm};
    margin: 0 18px;
    padding: 1rem 0;
    min-height: 1rem;
}

@media (max-width: 914px) and (min-width:428px) {
    margin: 0 10px;
}
`

const ContentContainer = styled.div`
width: 100%;
height: 100%;
margin-top: 100px;
display: flex;
justify-content: center;
align-items: center;

@media (max-width: 914px){
    margin-top: 50px;
    margin-bottom: 100px;
}


`

const Rarity = () => {
    return(
        <Section>
            <Container>
                <SubContainer id="gallery">
                     <Box>
                         <Subtext>Mint Date</Subtext>
                         <DateTitle>04.15.22</DateTitle>
                     </Box>
                     <Box>
                         <Subtext>Price</Subtext>
                         <DateTitle>Sold Out</DateTitle>
                     </Box>
                     <Box>
                         <Subtext>Supply</Subtext>
                         <DateTitle>9,999</DateTitle>
                     </Box>
                </SubContainer>
                <Title>
                    Gallery
                </Title>
                {/* <ThemeProvider theme={dark}>
                    <Button text="GO TO RARITY" link="/rarity" />
                </ThemeProvider> */}
               <ContentContainer>
                    <Carousel />
               </ContentContainer>

            </Container>
        </Section>
    )
}

export default Rarity