import React from "react";
import styled from 'styled-components';
import Button from '../../functions/Button';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import About2Background from '../../assets/img/Club.jpg';

const Section = styled.section`
min-height: 150vh;
width: 100%;
background-color: ${props => props.theme.text};
background-image: url(${About2Background});
background-repeat: no-repeat;
background-size: cover;
background-position: center 76%;
display: flex;
justify-content: center;
align-items: center;
position: relative;

@media (max-width:428px){
    min-height: 100%;
    background-size: cover;
    height: 460px;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
    padding: 100px 0 20px;
}

@media screen and (min-width: 915px){
    padding: 140px 0 30px;
}
`

const Container = styled.div`
width: 85%;
min-height: 120vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
flex-direction: column;
justify-content: space-between;
align-items: center;

@media (max-width:428px){
    min-height: 100%;
    width: 100%;
    padding: 43px 10px 0;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}
`

const BoxTop = styled.div`
height: 100%;
display: block;
`

const BoxBottom = styled.div`
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
margin: 0 auto;
width: 95%;

> a{
    margin-top: 30px;
    margin-bottom: 50px;
}

@media (max-width: 428px){
    height: 200px;
    a{
        margin-top: 10px;
        margin-bottom: 10px;
        padding: 0.75rem 1.15rem;
    }
}

@media (max-width: 914px) and (min-width: 429px) and (orientation: landscape){
    margin: 147px auto 0;
}
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
font-family: "Floyd", sans-serif;
text-align: center;
margin-top: 30px;

@media (max-width:428px){
    font-size: ${props => props.theme.fontxlx};
    line-height: 2.24rem;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    margin-bottom: 227px;
}
`

const Content = styled.p`
font-size: ${props => props.theme.fontlg};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 1rem auto;
font-weight:400;
text-align: center;

@media (max-width: 280px){
    word-break: break-all;
}

@media (max-width:428px){
    font-size: 0.8em;
    margin: 0 auto 0.5rem;
}
`

const Subtext = styled.h3`
font-size: ${props => props.theme.fontxl};
font-family: "Barlow Condensed Medium", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
font-weight: 400;
margin-bottom: 1rem;
align-self: center;
text-align: center;

@media (max-width:428px){
    font-size: 1em;
}
`

const About2 = () => {
    return(
        <>
        <Section id="madDogs">
            <Container>
                <BoxTop>
                    <Title>About The Mad Dogs</Title> 
                </BoxTop>
                <BoxBottom>
                    <Subtext>A collection of 9,999 unique dogs that will be speeding around the Metaverse with the meanest cars on the block.</Subtext>
                    <Content>Policy ID: 7e605894cdcbe8d1933f0a8760d93bd121aca583cf21ad1c25e51fd3 </Content> 
                    <ThemeProvider theme={dark}>
                    <Button text="VISIT JPG.STORE" link="https://www.jpg.store/collection/maddogcarclub" />
                    </ThemeProvider> 
                </BoxBottom>
            </Container>
        </Section>
        </>
    )
}

export default About2