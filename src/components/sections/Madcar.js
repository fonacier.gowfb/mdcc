import React from "react";
import styled from "styled-components";
// import MadCarBackground from "../../assets/img/MDCC_Car.gif";
import MadCarBackground from "../../assets/img/CarCollections.png";
import { Col } from "react-bootstrap";


const Section = styled.section`
min-height: 100vh;
width: 100%;
background-color: rgba(0, 0, 0, 0.4);
// background-blend-mode: darken;
background-image: url(${MadCarBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 54%;
// background-attachment: fixed;
display: flex;
justify-content: center;
align-items: center;
position: relative;
padding-top: 80px;

@media (max-width: 428px){
    min-height: 100%;
    padding: 43px 10px 125px;
    width: 100%;
    background-position: center;
    background-attachment: unset;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    align-items: flex-start;
    min-height: 43vh;
    padding: 20px 0 50px;
}

@media screen and (max-width: 1024px) and (min-width: 915px) {
    min-height: 90vh;
    padding: 100px 0;
}
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: flex-start;

@media (max-width: 428px){
    width: 100%;
    min-height: 100%;
    padding: 50px 10px;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}

@media screen and (max-width: 1024px) and (min-width: 915px) {
    min-height: 100%;
}
`

const Box = styled.div`
width: 90%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;


`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxxl};
text-transform: uppercase;
color: ${props => props.theme.body};
font-family: "Floyd", sans-serif;
text-align: center;
padding-top: 10px;
margin-top: -30px
align-self: center;

@media (max-width:428px){
    width: 100%;
    font-size: 2em;
    line-height: 2.24rem;
    margin-top: -70px;
}

@media (max-width: 280px){
    font-size: 1.5em;
    line-height: 2rem;
}
`

const VideoMadCarBackground = styled.video`
    width: 100vw;
    min-height: 80vh;
    object-fit: cover;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: -1;

    @media (max-width: 428px){
        min-height: 100%;
        background-position: center;
        background-attachment: unset;
    }
`

const MadCar = () => {
    return(
        <>
        <div id="madCars"></div>
        <div></div>
        <Section>
        {/* <VideoMadCarBackground autoPlay loop muted playsinline src={MadCarBackground} type="video/mp4"/> */}
            <Container>
                <Box>
              
                   <Title>Car Collections</Title>
                 
                </Box>
            </Container>
        </Section>
        </>
    )
}

export default MadCar