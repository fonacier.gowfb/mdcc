import React from "react";
import styled from 'styled-components';
import Button from '../../functions/Button';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import AboutBackground from '../../assets/img/About.jpg';
import ImgGallery from "../../functions/ImgGallery";
import Avatar from "../../assets/img/avatar.png";

const Section = styled.section`
min-height: 80vh;
width: 100%;
background-color: ${props => props.theme.text};
background-image: url(${AboutBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 55%;
display: flex;
justify-content: center;
align-items: center;
position: relative;

@media screen and (max-width: 428px) {
    min-height: 100%;
    background-size: 135%;
    background-position: 50% -1px;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
    padding: 50px 0;
    background-size: auto;
    background-position: center 61%;
}
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;

@media screen and (max-width: 428px) {
    width: 100%;
    padding: 10px 10px;
    min-height: 100%;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}
`
const BoxTop = styled.div`
height: 100%;
display: block;
`

const Box = styled.div`
width: 86%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
margin-top: 60px;
margin-bottom: 50px;

img{
    position: absolute;
    bottom: 20px;
    right: 20px;
    width: 260px;
    transform: rotate(25deg);
}

@media screen and (max-width: 428px) {
    font-size: ${props => props.theme.fontmd};
    img{
        position: absolute;
        bottom: 20px;
        right: 20px;
        width: 119px;
        transform: rotate(25deg);
    }
}

@media (max-width: 914px) and (min-width: 429px) {
    img{
        bottom: 33px;
        right: 32px;
        width: 172px;
    }
}

@media (max-width: 914px) and (min-width: 429px) and (orientation: landscape) {
    img{
        bottom: 50px;
        right: 50px;
        width: 167px;
    }
  }

  @media (max-width: 1024px) and (min-width: 915px) {
    img{
        right: 37px;
        width: 180px;
    }
}
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
color: ${props => props.theme.body};
font-family: "Floyd", sans-serif;
text-align: center;
margin-top: 30px;
margin-bottom: 30px;
align-self: center;

@media screen and (max-width: 428px) {
    font-size: ${props => props.theme.fontlx};
    margin-top: 0;
    font-family: "Barlow Condensed Bold", sans-serif;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    // font-size: ${props => props.theme.fontlg};
}
`

const SubText = styled.h3`
font-size: ${props => props.theme.fontxl};
font-family: "Barlow Condensed Medium", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
font-weight: 400;
margin-bottom: 1rem;
align-self: center;
text-align: center;

@media (max-width:428px){
    font-size: 1em;
}
`

const ButtonContainer = styled.div`
margin: 0 auto;
align-self: flex-start;
`

const About = () => {
    return(
        <Section id="about">
            <Container>
                {/* <Box><Carousel /></Box> */}
                <Box>
                    <Title>Hop In, Enjoy The Ride.</Title> 
                    <SubText>
                    Mad Dog Car Club is a club for dog and car lovers alike, with a welcoming community and a destination of sustainability through the symbiosis of our NFT collections, MD token, and P2E Game.
                    </SubText>
                    {/* <SubText>
                    Our ultimate goal is for Mad Dog Car Club to become a distinct brand in both the Metaverse and the real world. With endless possibilities of the Metaverse, Mad Dog Car Club has long-term goals to make sure our club will be one of the prominent faces of the NFT space.
                    </SubText> */}
                    <ButtonContainer>
                    <ThemeProvider theme={dark}>
                    <Button text="JOIN THE CLUB" link="https://discord.com/invite/maddogcarclub" />
                    </ThemeProvider>
                    </ButtonContainer>
                    <ImgGallery link={Avatar} text="MDCC"/>
                </Box>
            </Container>
        </Section>
    )
}

export default About