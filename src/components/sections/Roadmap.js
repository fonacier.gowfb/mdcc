import React from "react";
import styled from 'styled-components';
import Button from '../../functions/Button';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import DrawSvg from "../../functions/DrawSvg";
import RoadmapBackground from "../../assets/img/Background.jpg";
import mapImg from "../../assets/img/MDCC_Website_Roadmap.png";
import { AnimationOnScroll } from 'react-animation-on-scroll';
import TearPaperBackground from '../../assets/img/Paper_Tear.png';
import "animate.css/animate.min.css";



const Section = styled.section`
min-height: 100vh;
width: 100vw;
position: relative;
padding-top: 1px;
overflow: hidden;


background-color: ${props => props.theme.text};
background-image: url(${RoadmapBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;

img{
    width: 75%;
    display: block;
    margin: 0 auto;
    position: relative;
    z-index: 1;
}

div.button{
    width: 100%;
    display: flex;
    justify-content: center;
}

@media (max-width:428px){
    min-height: 100%;
    padding: 50px 20px 0px 50px;

    img{
        width: 560px;
        left: -81px;
        position: absolute;
        display: flex;
    }
}
`

const Title = styled.h1`
font-size: ${props => props.theme.fontxxxl};
font-family: "Floyd", sans serif;
font-weight: normal;
text-transform: uppercase;
color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
margin: 50px auto;
padding: 163px 0 0;
width: fit-content;

@media (max-width: 914px){
    font-size: ${props => props.theme.fontxlx};
    margin: 0 auto;
    padding: 20px 0 20px;
    text-align: center;
}
`

// const TitleTeam = styled.h1`
// font-size: ${props => props.theme.fontxxxl};
// font-family: "Floyd", sans serif;
// font-weight: normal;
// text-transform: uppercase;
// color: ${props => props.theme.body};
// display: flex;
// justify-content: center;
// align-items: center;
// margin: 0 auto;
// padding: 115px 0 50px;
// width: fit-content;
// `

const Container = styled.div`
width: 70%;
height: 100%;
margin: -121px auto 0;
display: flex;
justify-content: center;
align-items: center;
position: relative;
margin-bottom: 50px;

@media(max-width: 914px){
    width: 100%;
}

@media(max-width: 1024px) and (min-width: 429px){
    width: 98%;
}
`

const SvgContainer = styled.div`
display: flex;
justify-container: center;
align-items: center;
`
// const TeamContainer = styled.div`
// width: 77%;
// min-height: 100vh;
// margin: 0 auto 0;
// display: flex;
// justify-content: center;
// align-items: center;
// position: relative;
// `

// const team =styled.div`
// width: 77%;
// min-height: 100vh;
// margin: 0 auto 0;
// display: flex;
// justify-content: center;
// align-items: center;
// position: relative;
// `
const Items = styled.div`
width: 100%;
height: 100%;
margin-top: 150px;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
position: relative;

div.animate__animated{
    width:100%;
}

// div.long-content::before{
//     top: %52;
// }

// div.long-content-right::after{
//     top:52%;
// }
// div.long-content-publicsale::after{
//     top:55%;
// }

@media(max-width: 428px){
    margin-top: 358px;
}

@media (max-width: 914px) and (min-width: 429px){
    // div.long-content-right::after{
    //     top:51%;
    // }

    // div.launch-celebration::after{
    //     top:52%;
    // }
}
`

const ItemRight = styled.div`
width: 50%;
height: 100%;
display: flex;
flex-direction: column;
align-items: end;
justify-content: center;
float: right;

::before{
    content: "";
    position: absolute;
    // top: 50%;
    left: 50%;
    -webkit-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    transform: translateX(-50%);
    width: 1.25rem;
    height: 1.25rem;
    border-radius: 50%;
    background-color: #fff;
}

::after{
    content: "";
    position: absolute;
    // top: 53%;
    right: 44%;
    width: 54px;
    height: 2px;
    background-color: #fff;
}

@media(max-width: 428px){
    width: 100%;

    ::before{
        display: none;
    }
    
    ::after{
        display: none;
    }
    .long-content-right::after{
        top:unset;
        display: none;
    }
    .long-content-publicsale::after{
        top:unset;
        display: none;
    }
}

@media (max-width: 914px) and (min-width: 429px){

    div > p{
    padding: 0 10px 0 75px;
    text-align: left;
    }

    div div > p{
        padding: 0;
        text-align: left;
        }

    ::after{
        right: 42%;
    }
}
`
const ItemLeft = styled.div`
width: 50%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: end;
text-align: right;

float: left;

::after{
    content: "";
    position: absolute;
    // top: 50%;
    right: 48%;
    -webkit-transform: translateX(-50%);
    -ms-transform: translateX(-50%);
    transform: translateX(-50%);
    width: 1.25rem;
    height: 1.25rem;
    border-radius: 50%;
    background-color: #fff;
}

::before{
    content: "";
    position: absolute;
    // top: 53%;
    left: 44%;
    width: 54px;
    height: 2px;
    background-color: #fff;
}

@media(max-width: 428px){
    width: 100%;
    align-items: start;
    text-align: left;

    ::after{
        display: none;
    }
    
    ::before{
        display: none;
    }
    
    .long-content::before{
        display: none;
    }
}

@media (max-width: 914px) and (min-width: 429px){
    // .long-content::before{
    //     top: 51.5%;
    // }

    // .long-content-mdcl::before{
    //     top:52%;
    // }

    div > p{
    padding: 0 75px 0 10px;
    text-align: right;
    }

    div div > p{
        padding: 0;
        text-align: right;
        }

    ::before{
        left: 43%;
    }
}
`

const ItemContainer = styled.div`
width:100%;
height: fit-content;
padding: 0;
`

const Box2 = styled.div`
width:20%;
margin: 0 40px;
padding: 20px 0 0;
min-height: 170px;
background-image: url(${TearPaperBackground});
background-repeat: no-repeat;
background-size: auto;
background-position: center 54%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

@media (max-width:428px){
    width:95%;
    font-size: ${props => props.theme.fontsm};
    background-size: cover;
    margin: 0 18px;
    padding: 1rem 0;
    min-height: 1rem;
}

@media (max-width: 914px) and (min-width:428px) {
    margin: 0 10px;
    width: 27%;
}
`
const Box = styled.div`
height: fit-content;
color: ${props => props.theme.body};
padding: 0 80px;
position: relative;

@media(max-width: 428px){
    padding: 0;
}
`

const SubTitle = styled.p`
display: block;
font-size: ${props => props.theme.fontxl};
font-family: "Barlow Condensed Bold", sans serif;
text-transform:uppercase;
color: ${props => props.theme.body};
font-weight: bold;


@media (max-width: 428px){
    width: 200px;
    font-size: ${props => props.theme.fontlg};
    line-height: 1.25rem;

    ::after{
        content: "";
        position: absolute;
        top: 0;
        right: 16px;
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%);
        width: 1.25rem;
        height: 1.25rem;
        border-radius: 50%;
        background-color: #fff;
    }
    
    ::before{
        content: "";
        position: absolute;
        top: 8px;
        right: 37px;
        width: 123px;
        height: 2px;
        background-color: #fff;
    }
}
`

const SubTitleDone = styled.p`
display: block;
font-size: ${props => props.theme.fontxl};
text-transform:uppercase;
font-family: "Barlow Condensed Bold", sans serif;
text-decoration: line-through;
color: ${props => props.theme.body};
font-weight: bold;


@media (max-width: 428px){
    width: 200px;
    font-size: ${props => props.theme.fontlg};
    line-height: 1.25rem;

    ::after{
        content: "";
        position: absolute;
        top: 0;
        right: 16px;
        -webkit-transform: translateX(-50%);
        -ms-transform: translateX(-50%);
        transform: translateX(-50%);
        width: 1.25rem;
        height: 1.25rem;
        border-radius: 50%;
        background-color: #fff;
    }
    
    ::before{
        content: "";
        position: absolute;
        top: 8px;
        right: 37px;
        width: 123px;
        height: 2px;
        background-color: #fff;
    }

    .line1::before{
        width: 213px;
    }
}
`

const Text = styled.p`
display: block;
font-size: ${props => props.theme.fontlg};
color: ${props => props.theme.body};

font-weight:400;
margin: 0.5rem 0;


@media (max-width: 428px){
    width: 75%;
    font-size: ${props => props.theme.fontmd};
    line-height: 1.25rem;
}
` 

const TextDone = styled.p`
display: block;
font-size: ${props => props.theme.fontlg};
text-decoration: line-through;
color: ${props => props.theme.body};

font-weight:400;
margin: 0.5rem 0;


@media (max-width: 428px){
    width: 75%;
    font-size: ${props => props.theme.fontmd};
    line-height: 1.25rem;
}
` 

const SectionTitle = styled.p`
font-size: 2em;
font-family: "Floyd", sans serif;
font-weight: normal;
text-transform: uppercase;
color: ${props => props.theme.btnColor};
margin-bottom: 50px;
width: 100%;
height: 100%;
display: block;
padding: 0 80px;

@media (max-width: 428px){
    width: 75%;
    font-size: 1.5em;
    line-height: 1.25rem;
    padding: 0;
}
`

const SubTitleTeam = styled.h2`
font-size: 1.5em;
font-family: "Barlow Condensed Bold", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 66%;
margin: 13rem auto;
font-weight:400;
text-align: center;
margin-bottom: 30px;
br{
    display: none;
}

@media (max-width: 428px){
    width: 100%;
    margin: 1rem 0;
    br{
        display: block;
    }
}
`

// const SubTextTeam = styled.p`
// font-size: ${props => props.theme.fontlg};
// color: ${props => props.theme.body};
// align-self: flex-start;
// width: 100%;
// margin: 1rem auto 0 ;
// font-weight:400;
// text-align: center;
// padding-bottom: 50px;

// @media (max-width: 428px){
//     font-size: ${props => props.theme.fontmd};
//     padding-bottom: 0;
// }

// @media (max-width: 914px) and (min-width: 429px){
//     padding: 0 50px;
// }
// `
// const ButtonContainer = styled.div`
// margin: 0 auto;
// align-self: flex-start;
// `

const SubContainer = styled.div`
width: 100%;
// height: 100%;
// margin-top: -73px;
// margin-bottom: 50px;
height: 0;
margin-top: 0;
position: absolute;
z-index: 999;
padding: 0 10px;
display: inline-flex;
justify-content: center;
align-items: center;

@media (max-width:428px){
    padding: 0;
    // margin-top: -28px;

    margin-top: 0;
    margin-bottom: 0;
    height: 0;
    position: absolute;
    z-index: 999;
}

@media (max-width:914px) and (min-width:429px){
    justify-content: space-around;
}
`
const Subtext = styled.h3`
font-family: "Barlow Condensed Medium", sans serif;
font-size: ${props => props.theme.fontlg};
text-transform: uppercase;
color: ${props => props.theme.text};
margin-bottom: 1rem;
width: 80%;
align-self: center;
text-align: center;

@media (max-width:428px){
    font-size: ${props => props.theme.fontsm};
    margin-bottom: 5px;
}
`
const DateTitle = styled.h2`
font-size: ${props => props.theme.fontlx};
text-transform: uppercase;
width: 100%;
color: ${props => props.theme.btnColor};
align-self: flex-start;
font-family: "Floyd", sans-serif;
span{
    text-transform: uppercase;
    font-family: 'Akaya Telivigala', cursive;
}

text-align: center;
margin-bottom: 20px;

@media (max-width:428px){
    font-size: ${props => props.theme.fontsm};
    margin-bottom: 5px;
}
`


const Roadmap = () => {
     return(
         <>
         <SubContainer>
            
            <Box2>
            
                <Subtext>#1</Subtext>
                <DateTitle>06.10.22</DateTitle>
             
            </Box2>

            <Box2>
         
                <Subtext>#2</Subtext>
                <DateTitle>07.10.22</DateTitle>
            
            </Box2>

            <Box2>
          
                <Subtext>#3</Subtext>
                <DateTitle>08.10.22</DateTitle>
              
            </Box2>
        </SubContainer>
        <Section>           

            <SubTitleTeam>All 3 collections will be under the same policy ID. There will be a total of 18,000 car parts released (6,000 per drop), and only 6,000 Cars will be assembled (2,000 per drop). </SubTitleTeam>
            <Title id="roadmap">Roadmap</Title>            
            <img src={mapImg} alt="Road Map"/>
            <Container>
                <SvgContainer>
                    <DrawSvg />
                </SvgContainer>
                <Items>
                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight >
                        <ItemContainer>
                            <SectionTitle>Pre-Launch</SectionTitle>
                            <Box >
                                <SubTitleDone>MDCC Store</SubTitleDone>
                                <TextDone>We are releasing our clothing line, including t-shirts, hoodies, and other accessories, available to our community through our website. <br/><br/><br/></TextDone>
                            </Box>
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <ItemLeft className="long-content-mdcl">
                        <ItemContainer>
                                <SectionTitle>Mad Dog Collection Launch</SectionTitle>
                            <Box>
                                <SubTitleDone>Mint Day</SubTitleDone>
                                <TextDone>On <b>April 15th at 3pm EST</b>, a total of <b>9,999</b> unique Mad Dogs will be minted for 30 ADA each to our exclusive whitelisted community members. <br/><br/><br/></TextDone>
                            </Box>
                        </ItemContainer>
                    </ItemLeft>
                </AnimationOnScroll>
                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight className="long-content-publicsale">
                    <ItemContainer>
                            <Box>
                                
                                <SubTitleDone>The Public Raffle</SubTitleDone>
                                <TextDone>There will be a public raffle on <b>April 18th at 3pm EST</b>.<br/><br/><br/></TextDone>
                            </Box>
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <ItemLeft>
                        <ItemContainer>
                            <Box>
                                <SubTitleDone>Buy-Backs</SubTitleDone>
                                <TextDone>If any Mad Dogs are listed under the mint price on any Cardano NFT Marketplace, our management team will purchase them back and save them for future giveaways and promotions for our Mad Dog holders.<br/><br/><br/></TextDone>
                            </Box>
                        </ItemContainer>
                    </ItemLeft>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight className="launch-celebration">
                    <ItemContainer>
                            <Box>
                            
                                <SubTitleDone>Launch Celebration</SubTitleDone>
                                <TextDone>After a successful launch, we will randomly select <b>five</b> Mad Dog holders to receive <b>1,000 ADA each</b> and another <b>ten</b> holders to receive MDCC merch.<br/><br/></TextDone>
                                <TextDone>MDCC will be donating <b>5,000 USD</b> to the American Society for the Prevention of Cruelty to Animals (ASPCA). <br/><br/><br/></TextDone>
                            </Box>
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight className="long-content-right">
                    <ItemContainer >

                            <SectionTitle>Post Launch</SectionTitle>
                            <Box>
                            
                                
                                <SubTitle>EXCLUSIVE MONTHLY REWARDS FOR MAD DOG MEMBERS</SubTitle>
                                <Text><b>25% of royalties</b> will be released to the <b>top ranked 1000 Mad Dog holders</b> every month. <br/><br/></Text>
                                <Text>In addition, <b>two</b> randomly selected marketplace transactions will receive <b>1,000 ADA</b> each every month.<br/><br/><br/></Text>
                            </Box> 
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <ItemLeft>
                        <ItemContainer>
                            <Box>
                                <SubTitle>Real-World Utility</SubTitle>
                                <Text>MDCC members have access to exclusive events worldwide to connect with our community, check out hot cars, and access mad parties that only MDCC members can enjoy.<br/><br/></Text>
                                <Text><b>Upcoming Events:</b></Text>
                                <Text><b>September 10th (Los Angeles, CA)</b></Text>
                                <Text>- Our first MDCC Event for our club members will be hosted.</Text>
                                <Text><b>October 8th-9th (Las Vegas, NV)</b></Text>
                                <Text>- CNFTcon<br/><br/><br/></Text>
                            </Box>
                        </ItemContainer>
                    </ItemLeft>
                </AnimationOnScroll>


                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight className="long-content">
                        <ItemContainer>
                                <SectionTitle>MDCC Future Releases</SectionTitle>
                            <Box>
                            
                                <SubTitle>Car Collections</SubTitle>
                                <Text>MDCC wil be dropping 3 car collections:</Text>
                                <Text><b>June 10th (Collection #1)</b><br/></Text>
                                <Text><b>July 10th (Collection #2)</b><br/></Text>
                                <Text><b>August 10th (Car Collection #3)</b><br/><br/></Text>
                                <Text>There will be a total of 18,000 car parts released (6,000 per drop), and only 6,000 Cars will be assembled (2,000 per drop).<br/><br/><br/> </Text>
                            </Box>
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                <AnimationOnScroll animateIn="animate__fadeInLeftBig">
                    <ItemLeft>
                        <ItemContainer>
                            <Box>
                                <SubTitle>Mad Dog Token</SubTitle>
                                <br/><br/>
                            </Box>
                        </ItemContainer>
                    </ItemLeft>
                </AnimationOnScroll>

               
                <AnimationOnScroll animateIn="animate__fadeInRightBig">
                    <ItemRight>
                        <ItemContainer>
                            <Box>
                            <SubTitle>MD Racing</SubTitle>
                                <Text>MDCC’s very own P2E game takes advantage of our NFT collections and MD Token, providing an adrenaline pumping experience with high stakes rewards. Check out MD Racing for more information. </Text>
                                <Text>Launching Q4 2022<br/></Text>
                            </Box>
                        </ItemContainer>
                    </ItemRight>
                </AnimationOnScroll>

                
               

                </Items>
            </Container>
            {/* <Title id="team">Meet the Team</Title>
            <SubTitleTeam>Mad Dog Car Club is founded by a team of car enthusiasts from<br/> all over the world with a love<br/>for dogs.</SubTitleTeam>
            <div className="button">
            <ThemeProvider theme={dark}>
                    <Button text="CAREERS" link="/careers" />
            </ThemeProvider>
            </div>
             */}
        
            {/* <SubTextTeam>Our team of passionate individuals comes from various industries, from design to development to marketing. </SubTextTeam> */}
            

        </Section>
        </>
    )
}

export default Roadmap