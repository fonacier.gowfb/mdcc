import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';
import React, { useLayoutEffect, useRef } from 'react';
import styled from 'styled-components';
import FaqBackground from "../../assets/img/Background.jpg";
import SocialIcon from '../../functions/SocialIcon';
import Accordion from '../../functions/Accordion';
import { HashLink as Link } from 'react-router-hash-link';

const Section = styled.section`
min-height: 80vh;
width: 100%;
background-color: ${props => props.theme.text};
background-image: url(${FaqBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 54%;
display: flex;
justify-content: center;
align-items: center;
flex-direction: column;
position: relative;

@media (max-width: 428px){
  min-height: 100%;
  padding: 50px 0;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
  min-height: 100%;
  padding: 100px 0 0;
}
`

const Title = styled.h2`
  font-size: ${(props) => props.theme.fontxxxl};
  text-transform: uppercase;
  color: ${(props) => props.theme.body};
  align-self: flex-start;
  font-family: "Floyd", sans-serif;
  width: 100%;
  margin: 0 auto;
  margin-top: 150px;
  text-align: center;

  @media (max-width: 48em){
  font-size: 2.5em;
  margin-top: 0;
  }
`

const Container = styled.div`
width: 75%;
margin: 2rem auto;

display: flex;
justify-content: space-between;
align-content: center;

@media (max-width: 64em){
  width: 80%;
  }
  @media (max-width: 48em){
  width: 90%;
  margin: 2rem auto;
  flex-direction: column;

  &>*:last-child{
    &>*:first-child{

    margin-top: 0;
}

  }
  }
`
const Box = styled.div`
width: 75%;
margin: 90px 90px;

@media (max-width: 64em){
  width: 90%;
  align-self: center;
  margin: 90px 90px 0;
  }

  @media (max-width: 428px){
    margin: 50px 0;
    }
`

const FooterContainer = styled.div`
width: 75%;
min-height: 30vh;
margin: 50px 50px;
display: flex;
justify-content: center;
align-items: center;

@media (max-width: 428px){
  min-height: 0;
  margin: 50px 50px 0;
  }
`

const FooterBox = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

div{
    margin-top: 20px;
}

div a img{
    width: 35px;
}
`

const FooterTitle = styled.p`
font-size: ${props => props.theme.fontmd};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: center;

a:hover{
  color:#85FF10;
}
`

const Faq = () => {

const ref = useRef(null);
gsap.registerPlugin(ScrollTrigger);
useLayoutEffect(() => {
  
  let element = ref.current;

  ScrollTrigger.create({
    trigger: element,
    start:'bottom bottom',
    end:'bottom top',
    pin:true,   
    pinSpacing:false, 
    scrub:1,
    // markers:true,
  })

  return () => {
    ScrollTrigger.kill();
  };
}, [])

  return (
    <Section ref={ref} id="faq">
    <Title>Got Questions?</Title>
    <Box>
  <Accordion ScrollTrigger={ScrollTrigger} title="Is there MDCC merch? " >
  Yes! Click here to buy some!
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="When is the car drop? " >
  Our Mad Dog Collection was released the weekend of April 15th, 2022.<br/><br/>
  Our MDCC Car Collections will be released as we execute our roadmap. Stay tuned for more information. 
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="How will the car collection work? " >
  There will be a total of 18,000 parts in total for all car collections but only 6,000 cars can be<br/>
  assembled. To assemble a car you will need the matching body, wheels, and the engine. 
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="Is there a whitelist? " >
  The whitelist for our Mad Dog Collection drop has officially closed.<br/><br/>
  There is a <span style={{fontWeight: 'bold'}}>NEW WHITELIST</span> for our Car Collections. Be sure to join our Discord to find out the requirements. 
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="How much is the mint for the car collection? " >
  The mint price is 60 ADA. 
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="What will the cars be utilized for?" >
  We will be releasing a Play 2 Earn game called  MAD DOG RACING (MD Racing).
  </Accordion>
  <Accordion ScrollTrigger={ScrollTrigger} title="What is the P2E Game Based on? " >
  <span style={{fontWeight: 'bold'}}>MD RACING</span> takes advantage of our NFT collections and MD Token, providing an adrenaline <br/>
  pumping experience with high stakes rewards. Members will be able to show off their Mad <br/>
  Dogs, customize their Cars, and best of all place bets on their races using MD Tokens.
  </Accordion>
 
</Box>

    <FooterContainer>
        <FooterBox>
            <FooterTitle>
            © MAD DOG CAR CLUB<br/>
            ALL RIGHTS RESERVED<br/>
            <Link to='/terms'>TERMS</Link> &#38; <Link to='/policy'>POLICY</Link> 
            </FooterTitle>
            <SocialIcon />
        </FooterBox>
    </FooterContainer>
    </Section>
  )
}

export default Faq