import React from 'react'
import styled from 'styled-components';
// import Button from '../../functions/Button';
// import { Link } from "react-router-dom";
import { HashLink as Link } from 'react-router-hash-link';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import TeamBackground from "../../assets/img/BackgroundBlack.jpg";

// import img1 from '../../assets/Nfts/md1.jpg';
import img1 from '../../assets/img/team/Centario_NFT.jpg'
import img2 from '../../assets/img/team/Jesko_NFT.jpg'
import img3 from '../../assets/img/team/Rover_NFT.jpg'
import img4 from '../../assets/img/team/Aston_NFT.jpg'
import img5 from '../../assets/img/team/LeMans_NFT.jpg'
import img6 from '../../assets/img/team/Shelby_NFT.jpg'
import img7 from '../../assets/img/team/McQueen_NFT.jpg'
import img8 from '../../assets/img/team/NotJosh_NFT.jpg'
import img9 from '../../assets/img/team/Kenny_NFT.jpg'
import img10 from '../../assets/img/team/EFO_NFT.jpg'


const Section = styled.section`
min-height: 100vh;
width: 100vw;
background-color: ${props => props.theme.text};
position: relative;
background-image: url(${TeamBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
overflow: hidden;

.button > a{
    display: inline-block;
    background-color: #85FF10;
    color: #202020;
    outline: none;
    border: none;
    font-size: 1.25em;
    font-family: "Barlow Condensed Bold",sans serif;
    padding: 0.9rem 2.3rem;
    border-radius: 0;
    cursor: pointer;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    position: relative;
}

img{
    width: 70%;
    display: block;
    margin: 0 auto;
    position: relative;
    z-index: 1;
}
div.button{
    width: 100%;
    display: flex;
    justify-content: center;
}

@media (max-width: 428px){
    padding: 0 20px 50px;
}
`

const Container = styled.div`
width: 75%;
margin: 2rem auto 0;
display: flex;
justify-content: center;
align-items: start;
flex-wrap: wrap;


@media (max-width: 64em){
width: 80%;
}
@media (max-width: 48em){
width: 90%;
justify-content: center;
}
`
const Item = styled.div`
width: calc(20rem - 4vw);
padding: 1rem 0;
color: ${props => props.theme.body};
margin: 2rem 1rem;
position: relative;
z-index: 5;
/* border: 2px solid ${props => props.theme.text};
border-radius: 20px; */
transition: all 0.3s ease;

&:hover{
    img{
        transform: translateY(-2rem) scale(1.0);
    }
}

@media (max-width: 30em){
width: 65vw;
    padding: 0;
    margin: 1rem 1rem;
}

@media (max-width: 914px) and (min-width: 429px){
    width: calc(20rem - 4vw);
        padding: 0;
        margin: 1rem 1rem;
    }
`

const ImageContainer = styled.div`
width: 100%;
margin: 0 0;
/* background-color:${props => props.theme.carouselColor};
border: 1px solid ${props => props.theme.text}; */
/* padding: 1rem; */
border-radius:20px;
cursor: pointer;

img{
    width: 100%;
    height: auto;
    transition: all 0.3s ease;
}
`

const Title = styled.h1`
font-size: ${props => props.theme.fontxxxl};
font-family: "Floyd", sans serif;
font-weight: normal;
text-transform: uppercase;
color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
margin: 0 auto;
padding: 20px 0;
width: fit-content;
margin-top: 100px;

@media (max-width: 914px){
    font-size: 2.125em;
    text-align: center;
    margin-top: 20px;
    width: 75%;
}
`
const Title2 = styled.h1`
font-size: ${props => props.theme.fontxxl};
font-family: "Floyd", sans serif;
font-weight: normal;
text-transform: uppercase;
color: #85FF10;
display: flex;
justify-content: center;
align-items: center;
margin: 0 auto;
padding: 50px 0 0;
width: fit-content;

@media (max-width: 914px){
    font-size: 1.5em;
    text-align: center;
    margin-top: -45px;
}
`

const Name = styled.h2`
font-family: "Barlow Condensed Bold", sans serif;
font-size: 45px;
display: flex;
align-items: center;
justify-content: center;
text-transform: uppercase;
color: ${props => `rgba(${props.theme.body},0.9)`};
margin-top: 1rem;

@media (max-width: 914px){
    font-size: ${props => props.theme.fontlx};
}
`

const Position = styled.h2`
font-size: 21px;
font-weight: normal;
display: flex;
align-items: center;
justify-content: center;
text-transform: capitalize;
color: ${props => `rgba(${props.theme.body},0.9)`};

@media (max-width: 914px){
    font-size: ${props => props.theme.fontmd};
    line-height: 1rem;
}
`

const Quote = styled.p`
font-size: 21px;
font-weight: normal;
display: flex;
align-items: center;
justify-content: center;
text-transform: capitalize;
color: ${props => `rgba(${props.theme.body},0.9)`};
text-align: center;

@media (max-width: 914px){
    font-size: ${props => props.theme.fontmd};
}
`
const SubTitleTeam = styled.h2`
font-size: 1.5em;
font-family: "Barlow Condensed Bold", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 66%;
margin: 1rem auto;
font-weight:400;
text-align: center;
margin-bottom: 30px;

br{
    display: none;
}

@media (max-width: 428px){
    width: 100%;
    margin: 1rem 0;
    font-size: 18px;
    br{
        display: block;
    }
}
`
const Text = styled.p`
font-size: 1.5em;
text-transform: normal;
color: ${props => props.theme.body};
align-self: flex-start;
width: 66%;
margin: 1rem auto;
font-weight:400;
text-align: center;


@media (max-width:428px){
    width: 70%;
    font-size: 0.9em;
    margin: 0 auto 0.5rem;
}
`

const SubTitleTeam2 = styled.h2`
font-size: 1.5em;
font-family: "Barlow Condensed Bold", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 66%;
margin: 1rem auto;
font-weight:400;
text-align: center;
margin-top: 6rem;
margin-bottom: 30px;
br{
    display: none;
}

@media (max-width: 428px){
    width: 100%;
    margin: 1rem 0;
    br{
        display: block;
    }
}
`



const MemberComponent = ({img, name=" ",position=" ",quote=" ",}) => {

    return (
        <Item>
            <ImageContainer>
            <img width={500} height={400}  src={img} alt={name} />
            </ImageContainer>
            <Name>{name}</Name>
            <Position>{position}</Position>
            <Quote>{quote}</Quote>
        </Item>
    )
}

const Team = () => {
    return (
        <div id='center'>
        <Section>
        <SubTitleTeam2>MD Racing will give “utility” a whole new meaning. Members will be able to show off their Mad Dogs, customize their Cars, and best of all place bets on their races using MD Tokens. But that is just the beginning, MD Racing will continuously evolve through community feedback, content packs, and future updates. 
</SubTitleTeam2>
        <SubTitleTeam>FUEL UP, IN-DEPTH BREAKDOWN COMING SOON… </SubTitleTeam>
        <Title2 id="team">LAUNCHING: Q4 2022</Title2>
        <Title>Meet the Team</Title>
        <SubTitleTeam>Mad Dog Car Club is founded by a team of car enthusiasts from all over the world with a love for dogs.</SubTitleTeam>
        <Text>Our team of passionate individuals comes from various industries from design to development to marketing. <br/><br/></Text>
            <div className="button">
            <ThemeProvider theme={dark}>
                    <Link to="/careers">CAREERS</Link>
            </ThemeProvider>
            </div>
            <Container>
               <MemberComponent img={img1}  name="Centario" position="Founder/Chief Executive Officer" quote=" “Live life fast” " />
               <MemberComponent img={img2}  name="Jesko" position="Co-Founder/Chief of Operations" quote=" “Doin donuts in the six yessirski” " />
               <MemberComponent img={img3}  name="Rover" position="Chief Design Officer" quote=" “Speed. I am Speed.” " />
               <MemberComponent img={img4}  name="Aston" position="Chief Engineering Officer" quote=" “Put the medal to the pedal”" />
               <MemberComponent img={img5}  name="Le Mans" position="Chief Financial Officer" quote= "“Centario couldn’t spell Centenario” " />
               <MemberComponent img={img6}  name="Shelby" position="Chief Marketing Officer" quote=" “Shake N Bake” " />
               <MemberComponent img={img7}  name="McQueen" position="Chief Administrative Officer" quote=" “Adrenaline junkie and speed dealer” " />
            </Container>
            <Title>Head Moderators</Title>
            <Container>
                <MemberComponent img={img8}  name="Not Josh" position="Chief Moderator" quote=" “I’m not superstitious, just a little stitious” " />
                <MemberComponent img={img9}  name="Kenzo" position="Chief Moderator" quote=" “No pressure, No diamonds” " />
                <MemberComponent img={img10}  name="Efoshizzle" position="Chief Moderator" quote=" “I don’t make sense, I make ADA” " />
            </Container>
        </Section>
        </div>
    )
}

export default Team