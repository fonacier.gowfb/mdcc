import React from "react";
import styled from "styled-components";
import MadCarBackground from "../../assets/img/MDCC_Car.gif";

const Section = styled.section`
min-height: 95vh;
width: 100%;
background-color: rgba(0, 0, 0, 0.4);
background-blend-mode: darken;
background-image: url(${MadCarBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center 54%;
background-attachment: unset;
display: flex;
justify-content: center;
align-items: center;
position: relative;

@media (max-width: 428px){
    min-height: 100%;
    background-position: center;
    background-attachment: unset;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 50vh;
    padding: 50px 0;
}

@media screen and (max-width: 1024px) and (min-width: 915px) {
    min-height: 50vh;
    padding: 100px 0;
    
}
@media screen and (max-width: 1024px) and (min-width: 768px) {
    background-size: 1024px;
    background-attachment: unset;
}
// @media screen and (max-width: 1145px) and (min-width: 1025px) {
//     background-size: 90em;
// 
}
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;

@media (max-width: 428px){
    width: 100%;
    min-height: 100%;
    padding: 50px 10px;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}

@media screen and (max-width: 1024px) and (min-width: 915px) {
    min-height: 100%;
}
`

const Box = styled.div`
width: 90%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
font-family: "Floyd", sans-serif;
text-align: center;
margin-top: 30px;
align-self: center;

@media (max-width:428px){
    font-size: ${props => props.theme.fontxl};
    line-height: 2.24rem;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
   
}
`

const VideoMadCarBackground = styled.video`
    width: 100vw;
    min-height: 80vh;
    object-fit: cover;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: -1;
    @media (max-width: 428px){
        min-height: 100%;
        background-position: center;
        background-attachment: unset;
    }
`

const MadCar = () => {
    return(
        <>
        <Section id="racing">
        {/* <VideoMadCarBackground autoPlay loop muted playsinline src={MadCarBackground} type="video/mp4"/> */}
            <Container>
                <Box>
                   <Title>Race. Wager. Win.</Title>
                </Box>
            </Container>
        </Section>
        </>
    )
}

export default MadCar