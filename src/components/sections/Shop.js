import React from "react";
import styled from "styled-components";
import Button from '../../functions/Button';
import { ThemeProvider } from "styled-components";
import {dark} from '../../styles/Themes';
import ShopBackground from '../../assets/img/MDCC_Stickers.jpg';
import { Link } from "react-router-dom";

const Section = styled.section`
min-height: 100vh;
width: 100%;
background-color: ${props => props.theme.text};
background-image: url(${ShopBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
background-attachment: unset;
display: flex;
justify-content: center;
align-items: center;
position: relative;

@media (max-width: 428px){
    min-height: 40vh;
    background-attachment: unset;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    background-attachment: unset;
    min-height: 50vh;
    padding: 50px 0;
    align-items: end;
}
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;

@media (max-width: 428px){
    min-height: 220px;
    align-items: flex-end;
}

@media screen and (max-width: 914px) and (min-width: 429px) {
    min-height: 100%;
}
`

const Box = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

a{
    padding: 1rem 4.3rem;
    font-family: "Barlow Condensed Bold", sans serif;
}

@media (max-width: 428px){
    justify-content: end;
    font-size: ${props => props.theme.fontlg};
    margin-bottom: 87px;

    a{
        padding: 0.45rem 2.6rem;
    }
}
@media screen and (max-width: 914px) and (min-width: 429px) {
    margin-bottom: 180px;
}
`

const Shop = () => {
    return(
        <Section id="shop">
            <Container>
                <Box>
                    <ThemeProvider theme={dark}>
                        <Button text="SHOP" link="https://mdccstore.com" />
                    </ThemeProvider>
                </Box>
            </Container>
        </Section>
    )
}

export default Shop