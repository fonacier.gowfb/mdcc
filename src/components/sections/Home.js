import React from "react";
import styled from 'styled-components';
// import CoverVideo from "../CoverVideo";
import TypeWriterText from "../../functions/TypeWriterText";
// import RoundTextBlack from "../../assets/Rounded-Text-Black.png";
// import { keyframes } from "styled-components";
import HomeBackground from '../../assets/MDCC_Car_Inside.gif';

const Section = styled.section`
min-height: ${props => `calc(100vh - ${props.theme.navHeight})`};
width: 100vw;
position: relative;
background-image: url(${HomeBackground});
background-size: cover;
background-repeat: no-repeat;

@media (min-width: 768px){
    min-height: 100%;
}

@media (max-width:428px){
    margin-top: 90px;
    min-height: 100%;
}
`

const Container = styled.div`
width: 85%;
min-height: 140vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;

@media (max-width:428px){
    min-height: 300px;
}

@media (min-width: 768px){
    min-height: 70vh;
}

@media (min-width: 1024px){
    min-height: 100vh;
}
`

const Box = styled.div`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
padding: 270px 0 0;

@media (max-width:428px){
    padding: 86px 0 0;
}
`

const Title = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
width: 80%;
color: ${props => props.theme.body};
align-self: flex-start;
font-family: "Floyd", sans-serif;
span{
    text-transform: uppercase;
    font-family: 'Akaya Telivigala', cursive;
}

.text-1{
    color: blue;
}

.text-2 {
    color: orange;
}

.text-3{
    color: red;
}

@media (max-width:428px){
    width: 100%;
    font-size: ${props => props.theme.fontxlx};
}
`

const SubTitle = styled.h3`
font-size: ${props => props.theme.fontxxl};
font-family: "Barlow Condensed Medium", sans serif;
text-transform: uppercase;
color: ${props => props.theme.body};
margin-bottom: 1rem;
width: 80%;
align-self: flex-start;

@media (max-width:428px){
    font-size: ${props => props.theme.fontlx};
}
`
const VideoBackground = styled.video`
    width: 100vw;
    min-height: ${props => `calc(100vh - ${props.theme.navHeight})`};
    object-fit: cover;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    z-index: -1;

    @media (min-width: 768px){
        min-height: 100%;
    }

    @media (max-width:428px){
        min-height: 300px;
    }
`

const Home = () => {
    return(
        <>
        <Section id="home">
            
        {/* <VideoBackground autoPlay loop muted playsinline src={HomeBackground} type="video/mp4"/> */}
            <Container>
                <Box>
                    <Title>Mad Dog Car Club</Title>
                    <SubTitle>All About The Drive</SubTitle>
                </Box>
            </Container>
        </Section>
        </>
    )
}

export default Home