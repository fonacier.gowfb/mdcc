import React, {useEffect, useState} from "react";
import styled from "styled-components";
import { ThemeProvider } from 'styled-components';
import GlobalStyles from '../../styles/GlobalStyles';
import "../../App.css";
import "../../fonts/Floyd.ttf";
import {light} from '../../styles/Themes';
import FooterBackground from '../../assets/img/Background.jpg';
import Navigation from "../../functions/Navigation2";
import SocialIcon from "../../functions/SocialIcon";
import ScrollToTop from '../../functions/ScrollToTop';
import { HashLink as Link } from 'react-router-hash-link';


const Section = styled.section`
min-height: 100vh;
width: 100%;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;

@media (max-width: 428px){
    min-height: 100%;
}
`

const FooterSection = styled.section`
width: 100%;
padding: 50px 0;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;
background-image: url(${FooterBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
`

const Container = styled.div`
width: 75%;
min-height: 80vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: start;
margin-top: 180px;
margin-bottom: 50px;

@media (max-width: 428px){
    min-height: 100%;
    margin-top: 100px;
    width: 95%;
}
`

const Box = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
margin-top: 50px;
margin-bottom: 50px;
`

// const BoxSort = styled.div`
// width: 20%;
// height: 100%;
// min-height: 500px;
// display: flex;
// flex-direction: column;
// justify-content: start;
// align-items: center;


// ul{
//     list-style: none;
//     padding-left: 0;

// }

// li {
//     padding: 10px 20px;
//     border-bottom: 2px solid #000;
//     font-weight: 600;
// }

// li:first-child{
//     border-top: 2px solid #000;
// }

// li a{
//     cursor:pointer;
// }

// li a:hover{
//     color:${props => props.theme.btnColor};
// }
// `


const BoxGallery = styled.div`
width: 80%;
height: 100%;
display: block;
`

const FooterContainer = styled.div`
width: 75%;
min-height: 20vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;
`

const FooterBox = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

div{
    margin-top: 20px;
}

div a img{
    width: 35px;
}
`

const FooterTitle = styled.p`
font-size: ${props => props.theme.fontmd};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: center;

a:hover{
    color:#85FF10;
  }
`

const BtnContainer = styled.div`
display: flex;
flex-direction: column;
justify-content: flex-start;
align-content: start;
width:100%;
padding-right: 10px;
`

// const GalleryWrapper = styled.div`

// `

// const CardItems = styled.div`
// div div > div.card{
//     background-color: transparent !important;
//     background-clip: border-box;
//     border: none;
//     border-radius: 0;
// }
// `


// const CardContent = styled.div`
// width: 200px;
// box-sizing: border-box;
// float: left;
// display: block;
// margin: 20px 10px;

// img {
//     width:200px;
// }
// `

const Title = styled.h2`
font-size: ${(props) => props.theme.fontxxl};
text-transform: uppercase;
color: ${(props) => props.theme.text};
font-family: "Floyd", sans-serif;
margin-bottom: 30px;
`
const SubText = styled.p`
font-size: ${props => props.theme.fontlg};
color: ${props => props.theme.text};
align-self: flex-start;
width: 100%;
margin: 1rem auto;
font-weight:400;
text-align: left;
`

const SubTitle = styled.h2`
font-size: ${props => props.theme.fontxxl};
text-transform: uppercase;
color: ${props => props.theme.text};
align-self: flex-start;
width: 100%;
margin: 10px auto;
font-weight:600;
margin-top: 80px;
`
const Content = styled.h2`
font-size: ${props => props.theme.fontxl};
text-transform: uppercase;
color: ${props => props.theme.text};
align-self: flex-start;
width: 100%;
margin: 10px auto;
font-weight:600;
margin-top: 80px;
`

const Terms = () => {
    // const [data, setData] = useState(Data);

    // const categoryData = Data.map((value)=>{
    //       return value.category
    //  });
    // const tabsData= ["All", ...new Set(categoryData)];
    
    // const filterCategory=(category) =>{
    //     if(category==="All"){
    //         setData(Data);
    //         return;
    //     }
    //    const filteredData =  Data.filter((value)=>{
    //        return value.category === category;
    //    })
    //    setData(filteredData);
    // }

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    return(
        <>
        <GlobalStyles />
        <ThemeProvider theme={light}>
            <Navigation />
            <Section id="home">
                <Container>
                <Box>
                    <Title>
                    Terms of Use
                    </Title>
                    <SubText>
                    These Terms of Use (“Agreement”) govern your access to and use of the websites (“Sites”) and mobile applications (“Apps”) operated by MRT & Co doing business as Mad Dog Car Club or “MDCC” and its affiliates, including the Sites at https://maddogcarclub.io/ (including all of its subdomains). The Web Services include, without limitation, for example, e-mail, push notifications and SMS/MMS communications. Collectively the Site and the App and Web Services are the “Platform”).
                    </SubText>
                    <SubText>
                    The MDCC entity that operates a Site or App is identified at that Site or App. The words “user,” “you” and “your” mean users of the Site or App, and the words “we,” “our” and “us” mean the MDCC entity that operates the Site or App.
                    </SubText>
                    <SubText>
                    Please read this Agreement carefully before accessing or using the Platform. This Agreement also includes our <Link to="/policy">PRIVACY POLICY</Link>, which is incorporated herein by reference. This Agreement constitutes a valid and binding legal agreement between you and MDCC. You may not access or use the Platform if you are not at least 13 years old. If you are under 18 or not of legal age to form a binding contract in your jurisdiction of residence, you must have you parent or legal guardian's permission to access or use the Platform. BY ACCESSING OR USING THE PLATFORM, YOU ACCEPT, WITHOUT LIMITATION OR QUALIFICATION, THE TERMS OF THIS AGREEMENT, INCLUDING THE [insert link to privacy notice], AND YOU REPRESENT AND WARRANT THAT YOU HAVE THE RIGHT, AUTHORITY, AND CAPACITY TO ENTER INTO THIS AGREEMENT.
                    </SubText>
                    <SubText>
                    We reserve the right to update this Agreement from time-to-time. Updates will be posted on the Platform. Any updates to this Agreement will supersede and replace the previous Agreement effective immediately upon posting. It is your responsibility to periodically review this Agreement for updates. If we update this Agreement, we may, but are not required to, notify you by e-mail at the last e-mail address you provided to us (if any). You are responsible for providing us with your most current e-mail address. Your use of our Platform following such update shall indicate your agreement to be bound by the terms and conditions of this Agreement as updated. IF YOU DO NOT ACCEPT AND AGREE TO ALL OF THE PROVISIONS OF THIS AGREEMENT AND THE  <Link to="/policy">PRIVACY POLICY</Link> AS THEY MAY BE UPDATED FROM TIME-TO-TIME, DO NOT ACCESS OR USE THE PLATFORM.
                    </SubText>
                    <SubTitle>Authorized Use of Platform</SubTitle>
                    <SubText>
                    We authorize you to view and download a single copy of content on the Platform, including content provided by PDF downloads, RSS or Atom feeds, REST-based web service XML, JSON feed, or other web feed, solely for your lawful, non-commercial and personal use as expressly permitted by and subject to the restrictions contained in this Agreement, including the requirement that you maintain all copyright and other proprietary notices contained in the Materials (as defined below).
                    </SubText>
                    <Content>Modification of Platform</Content>
                    <SubText>
                    We reserve the right, at any time, to modify, suspend, or discontinue the Platform or any part thereof with or without notice. You agree that we will not be liable to you or to any third party for any modification, suspension or discontinuance of the Platform or any part thereof and you understand and acknowledge that the Platform may contain inaccuracies or typographical errors. Any future release, update, modification or other addition to functionality of the Platform shall be subject to the terms of this Agreement.
                    </SubText>
                    <Content>Prohibited Uses</Content>
                    <SubText>
                    You acknowledge and agree that we specifically prohibit, and by your use of the Platform, you expressly agree not to engage in, the following conduct: the deletion or alteration of any Materials; any action that imposes an unreasonable or disproportionately large load on the infrastructure of the Platform; the use of any data mining, robots, automated downloading, scripted or headless browsers, scraping or other similar means or methods for purposes of data gathering or extraction; and/or the use of any directory information to post or transmit any unsolicited advertising, promotional materials, junk mail, spam, chain letters, pyramid schemes, telephone calls or other solicitations. You agree that you will not use any robot, spider or other automated device, process or means to access the Platform. You agree not to, intentionally or unintentionally, disrupt, overwhelm, attack, modify, reverse engineer, decompile, disassemble or otherwise interfere with the Platform or any associated software, hardware and/or servers in any way, and you agree not to impede or interfere with others' use of the Platform. You further agree that you will not (A) use any manual process to monitor or copy the Platform or for any other unauthorized purpose without our prior express written permission; and/or (B) use the Platform to intentionally or unintentionally violate any applicable local, state, national or international law, regulation or ordinance.
                    </SubText>
                    <SubText>
                    You may not copy, use, download, modify, frame, publish, download, transmit, retransmit, transfer, sell, license, reproduce, create derivative works of, distribute, perform, display, disseminate, rearrange, redistribute, alter, adapt, crop, resize, move, remove, delete or in any way exploit or make commercial use of, any of the Materials, in whole or in part, directly or indirectly, without the prior written consent, in each instance, of MDCC and/or the owner thereof, except as expressly permitted in this Agreement or under applicable law. If permission is granted by MDCC and/or by all other entities with an interest in the relevant intellectual property, you may not change or delete any author attribution, trademark, legend, copyright or other notice. Use of the Materials on any other website or other telecommunication media or in a networked computer environment for any purpose, without the prior express written permission of MDCC in each instance, is prohibited. Without limiting the foregoing and for the avoidance of doubt, you must abide by all copyright notices and other restrictions contained on the Platform.
                    </SubText>
                    <Content>Copyright; Prohibition Against Copying</Content>
                    <SubText>
                    The contents of the Platform, such as the compilation and arrangement of text, graphics, images, photographs and other materials, and the hypertext markup language (HTML), Cascading Style Sheets (CSS), scripts (JS), active server pages (ASP and ASPX), content provided by a RSS or Atom feed, web service feed or other web feed or other content or software used in or provided through the Platform (the “Materials”), are the exclusive property of MDCC, our licensors, or other content suppliers and are protected by copyright under United States and foreign laws. Unauthorized use of the Materials may violate copyright, trademark and other laws. Our provision of the Platform does not transfer to you or any third party any right, title or interests in or to the Materials, including any copyrights, patents, trademarks, trade secret or other intellectual property rights therein, subject to the licensed rights granted above.
                    </SubText>
                    <Content>Trademarks</Content>
                    <SubText>
                    The trademarks, service marks, logos, graphics, icons and other indicia of origin (collectively, the “Marks”) included, or made available, on or through the Platform are owned and/or registered by MDCC and third parties in the United States and other countries. All Marks not owned and/or registered by MDCC are the property of their respective owners. No license or right to use any Marks included, or made available, on or through the Platform is granted, whether by implication or otherwise, and any use of any Marks included, or made available, on or through the Platform is expressly prohibited unless authorized in writing by the owner of the applicable Mark(s). All rights not explicitly granted herein are reserved.
                    </SubText>
                    <SubTitle>Content and Communications</SubTitle>
                    <Content>Third Party Content</Content>
                    <SubText>
                    The Platform contains content that has been provided by third parties, including retailers, and over which we have no control, including information relating to sales, offers, coupons, giveaways, advertisements, promotions, pricing and product descriptions. The use, posting, distribution or publication of content on the Platform does not constitute or imply an endorsement, recommendation, advice, opinion or comment by us, nor is it an assurance of legality, quality or safety by us. Because prices and details of offers, promotions, specials and sales may significantly vary from the information posted on the Platform, you should visit the applicable third-party website in advance of purchase to confirm the accuracy of such information. Excluding any products purchased by you are purchased directly from the applicable third-party retailer and the third party retailer is solely responsible to you for every aspect of such purchase. You agree that MDCC will not be responsible for any loss or damage incurred as the result of any such interactions, including failure on the part of any third party to deliver the goods that you order in a satisfactory manner, nor for any products or services purchased from retailers via the Platform. This Section shall survive expiration or termination of this Agreement.
                    </SubText>
                    <Content>User Content</Content>
                    <SubText>
                    "User Content" means any and all information, content, feedback, ratings, reviews or other materials that a user posts, submits, displays publishes or transmits (hereinafter “post”) to other users or other persons on or through the Platform, including via chat bots, message boards, surveys, personal profiles, forums, bulletin boards, and other interactive features. You understand and acknowledge that all User Content will be considered non-confidential and non-proprietary. You further understand and acknowledge that you are solely responsible for all User Content that you post and that you, not MDCC, assume full responsibility for all risks associated with any User Content, including its legality, reliability, accuracy and appropriateness. We are not responsible or liable to any third party for any User Content posted by you or any other user, including any reliance on its accuracy or completeness. We are not obligated to backup any User Content and User Content may be deleted at any time. You hereby grant to MDCC, and our affiliates, licensees, successors and assigns, an irrevocable, non-terminable, nonexclusive, royalty-free, fully paid, worldwide license to use, reproduce, adapt, abridge, distribute, publicly display, modify, perform, prepare derivative works of, incorporate into other works, exploit and otherwise disclose to third parties your User Content for any purpose. You represent and warrant that (i) you own or have the necessary rights in and to your User Content to grant the license above, and (ii) all of your User Content will comply with this Agreement.
                    </SubText>
                    <Content>Content Standards</Content>
                    <SubText>
                    User Content must comply with all applicable federal, state, local and international laws, rules and regulations. Without limiting the foregoing, User Content must not: (i) contain any material which is deceptive, defamatory, obscene, indecent, abusive, offensive, harassing, violent, hateful, inflammatory or otherwise objectionable; (ii) promote sexually explicit or pornographic material, violence or discrimination based on race, gender, religion, nationality, disability, sexual orientation or age; (iii) infringe any patent, trademark, trade secret, copyright or other intellectual property or other rights of any person or entpolicyity; (iv) violate the legal rights (including the rights of publicity and privacy) of others or contain any material that could give rise to any civil or criminal liability under applicable laws or regulations or that may otherwise be in conflict with this Agreement and our <Link to="/policy">Privacy Policy</Link>; (v) promote any illegal activity or advocate, promote or assist any unlawful act; (vi) cause annoyance, inconvenience or needless anxiety or be likely to upset, embarrass, alarm or annoy any other person; (vii) impersonate any person, or misrepresent your identity or affiliation with any person or organization; (viii) involve commercial activities or sales, such as sweepstakes, contests and other sales promotions, barter or advertising; or (ix) give the impression that it emanates from or are endorsed by MDCC.
                    </SubText>
                    <Content>Monitoring and Enforcement</Content>
                    <SubText>
                    Although we are not obligated to screen communications or postings in advance and are not responsible for screening or monitoring the Platform, or any element or component thereof, you acknowledge and agree that we have the right to monitor the Platform in our sole discretion, and, notwithstanding anything to the contrary contained in the [insert link to privacy notice], to disclose information as necessary or appropriate to satisfy any law, regulation or other governmental request, to operate the Platform properly, or to protect itself or other users. If we are notified of communications or content that allegedly violates this Agreement and/or any applicable law, rule or regulation, we may investigate the allegation and determine in our sole discretion whether to remove, or request the removal of, the content or to terminate the use of the Platform by the user who posted such content, but we have no liability or responsibility to users for performance or nonperformance of such activities. Without limiting anything set out elsewhere in this Agreement, if a user views content or receives communications allegedly inconsistent with this Agreement, the user may send an e-mail to [insert link to email address] stating, in reasonable detail, the observed behavior. Following receipt of such correspondence, a decision will be made by us, in our sole discretion, regarding the appropriate action to take, if any, to address the concern set out in such e-mail. Any action or inaction by us does not imply or impose any responsibility or liability on us for the form, intent or accuracy of the original or any subsequent content posted by, or communications between, users. Without limiting the foregoing, we have the right to fully cooperate with any law enforcement authorities or court order requesting or directing us to disclose the identity or other information of anyone posting any User Content or other materials on or through the Platform. YOU WAIVE AND HOLD THE INDEMNIFIED PARTIES (AS DEFINED BELOW) HARMLESS FROM ANY CLAIMS RESULTING FROM ANY ACTION TAKEN BY ANY OF THEM DURING, OR AS A RESULT OF, OR AS A CONSEQUENCE OF INVESTIGATIONS OR DISCLOSURES BY ANY SUCH PARTIES OR LAW ENFORCEMENT AUTHORITIES PURSUANT TO THIS SECTION. This Section shall survive expiration or termination of this Agreement.
                    </SubText>
                    <Content>Third Party Sites</Content>
                    <SubText>
                    The Platform may have links to third party websites, services and applications, such as websites hosted by third party retailers (collectively, “Third Party Sites”). When you click on such a link, you will leave the Platform and be directed to the Third-Party Site. This Agreement does not apply to Third Party Sites. We have no control over, do not review or endorse, and are not responsible for any Third-Party Sites, the content therein, or their privacy policies. We encourage you to review the privacy policies of any Third-Party Sites you access.
                    </SubText>
                    <Content>Notice for Claims of Intellectual Property Violations and Agent for Notice</Content>
                    <SubText>
                    If you believe in good faith that any material in the Platform infringes your copyright, you (or your agent) may send our Agent for Notice of claims of copyright or other intellectual property infringement (“Agent”) a written notice requesting that we remove or block access to the infringing material. Your notice to us must include the following information:
                    <ol>
                        <li>an electronic or physical signature of a person authorized to act on behalf of the owner of the exclusive right that is allegedly infringed;</li>
                        <li>identification of the copyrighted work or other intellectual property that you claim has been infringed;</li>
                        <li>a description of where the material that you claim is infringing is located with sufficient detail (including applicable URL if possible) that we may find it;</li>
                        <li>your address, telephone number, and e-mail address;</li>
                        <li>a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright or intellectual property owner, its agent or the law;</li>
                        <li>information, if possible, sufficient to permit us to notify the owner/administrator of the allegedly infringing content; and</li>
                        <li>a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright or intellectual property owner or authorized to act on the copyright or intellectual property owner's behalf.</li>
                    </ol>
                    </SubText>
                    <SubText>
                    If you believe in good faith that someone has wrongfully filed a notice of copyright infringement against you, the Digital Millennium Copyright Act (the “DMCA”) permits you to send us a counter-notice. Notices and counter-notices must meet the then-current statutory requirements imposed by the DMCA; see <a href="http://www.loc.gov/copyright">www.loc.gov/copyright</a>. Notices and counter-notices should be sent to our Agent:
                    </SubText>
                    <SubText>
                    [insert information] DMAB LAW can provide the service.
                    </SubText>
                    <SubTitle>Additional Terms for App</SubTitle>
                    <Content>App Platforms</Content>
                    <SubText>
                    You acknowledge and agree that the App is dependent on a third-party mobile app platform (such as the Apple iTunes store) (each, an “App Platform”). Each App Platform may have its own terms and conditions to which you must agree before downloading the App from it. You agree to comply with, and your use of the App is conditioned upon your compliance with, all applicable agreements, terms and conditions of use/service, and other policies of the applicable App Platform.
                    </SubText>
                    <Content>Wireless Carrier Considerations</Content>
                    <SubText>
                    To use or otherwise access the App, you must have a mobile device that is compatible with the App. We do not warrant that the App will be compatible with your mobile device. We do not charge for the App, but your wireless carrier's normal messaging, data and other rates and fees will still apply.
                    </SubText>
                    <Content>Consent for Text and Mobile Messaging</Content>
                    <SubText>
                    By using the App, you expressly agree that we may communicate with you by SMS, MMS, text message, push notifications or other electronic means directed to your mobile device and that certain information about your usage of the App may be communicated to us. Our communications to you may include, among other things, special events, news, promotions and offers. In the event you change or deactivate your mobile device telephone number, you agree to promptly update your Account information to ensure that messages are not sent to the person that acquires your old number.
                    </SubText>
                    <Content>User End Licenses</Content>
                    <SubText>
                    We grant you a personal, limited, non-exclusive, non-transferable, non-sublicensable, revocable license to install and use one copy of the App downloaded directly from a legitimate marketplace (such as the Apple iTunes App store), solely in object code format and solely for your personal use for lawful purposes using your Account, on a single mobile device that you own or control. You may not:
                    <ul>
                        <li>modify, disassemble, decompile or reverse engineer the App, except to the extent such restriction is expressly prohibited by law;</li>
                        <li>rent, lease, loan, resell, sublicense, distribute or otherwise transfer the App to any third party;</li>
                        <li>make any copies of the App, other than for your own personal backup; or</li>
                        <li>remove, circumvent, disable, damage or otherwise interfere with security-related features of the App, features that prevent or restrict use or copying of any content accessible through the App, or features that enforce limitations on use of the App.</li>
                    </ul>
                    </SubText>
                    <SubText>
                    The foregoing limited license is not a sale of the App, and we otherwise retain all rights, title and interest in and to the App. We may terminate your license to the App at any time, in our sole discretion. Any attempt by you to transfer any of the rights, duties or obligations hereunder, except as expressly provided by this Agreement, is void.
                    </SubText>
                    <Content>Updates</Content>
                    <SubText>
                    You acknowledge and agree that from time-to-time we may issue updates to the App. If the settings on your device are set to automatically update the App, you expressly consent to such automatic updating, and agree that this Agreement (as it may be amended) will apply to all such updates.
                    </SubText>
                    <Content>iTunes App Terms</Content>
                    <SubText>
                    The following additional terms and conditions apply to you if you are using the App from iTunes. To the extent the other terms and conditions of this Agreement are less restrictive than, or otherwise conflict with, the terms and conditions of this Section, the more restrictive or conflicting terms and conditions of this Section apply, but solely with respect to the App from iTunes. MDCC and you acknowledge and agree that Apple, and Apple's subsidiaries, are third party beneficiaries of this Agreement, and that, upon your acceptance of the terms and conditions of this Agreement, Apple will have the right (and will be deemed to have accepted the right) to enforce this Agreement against you as a third party beneficiary thereof.
                    </SubText>
                    <SubText>
                    MDCC and you acknowledge that this Agreement is concluded between MDCC and you only, and not with Apple, and MDCC, not Apple, is solely responsible for the App and the content thereof. To the extent this Agreement provides for usage rules for the App that are less restrictive than the Usage Rules set forth for the App in, or otherwise is in conflict with, the iTunes Terms of Service, the more restrictive or conflicting iTunes Terms of Service usage rules apply.
                    </SubText>
                    <SubText>
                    MDCC and you acknowledge that Apple has no obligation whatsoever to furnish any maintenance and support services with respect to the App.
                    </SubText>
                    <SubText>
                    MDCC and you acknowledge that MDCC, not Apple, is responsible for addressing any claims of you or any third party relating to the App or your possession and/or use of the App, including: (i) product liability claims; (ii) any claim that the App fails to conform to any applicable legal or regulatory requirement; and (iii) claims arising under consumer protection or similar legislation.
                    </SubText>
                    <SubText>
                    MDCC and you acknowledge that, in the event of any third party claim that the App or your possession and use of the App infringes that third party's intellectual property rights, MDCC, not Apple, will be solely responsible for the investigation, defense, settlement and discharge of any such intellectual property infringement claim.
                    </SubText>
                    <SubText>
                    You represent and warrant that (i) you are not located in a country that is subject to a U.S. Government embargo, or that has been designated by the U.S. Government as a “terrorist supporting” country; and (ii) you are not listed on any U.S. Government list of prohibited or restricted parties.
                    </SubText>
                    <SubText>
                    You must comply with applicable third-party terms, such as the iTunes App Terms, when using the App.
                    </SubText>
                    <SubText>
                    This Section  shall survive expiration or termination of this Agreement.
                    </SubText>
                    <SubTitle>Additional Terms for NFT</SubTitle>
                    <Content>Definitions.</Content>
                    <SubText>
                    “Art” means any art, design, and drawings that may be associated with an NFT that you Own.
                    </SubText>
                    <SubText>
                    "NFT" means any blockchain-tracked, non-fungible token, such as those conforming to the ERC-721 standard.
                    </SubText>
                    <SubText>
                    “Own” means, with respect to an NFT, an NFT that you have purchased or otherwise rightfully acquired from a legitimate source, where proof of such purchase is recorded on the relevant blockchain.
                    </SubText>
                    <SubText>
                    “Extensions” means third party designs that: (i) are intended for use as extensions or overlays to the Art, (ii) do not modify the underlying Art, and (iii) can be removed at any time without affecting the underlying Art.
                    </SubText>
                    <Content>“Purchased NFT” means an NFT that you Own.</Content>
                    <SubText>
                    “Third Party IP” means any third party patent rights (including, without limitation, patent applications and disclosures), copyrights, trade secrets, trademarks, know-how or any other intellectual property rights recognized in any country or jurisdiction in the world.
                    </SubText>
                    <Content>Ownership.</Content>
                    <SubText>
                    You acknowledge and agree that MDCC (or, as applicable, its licensors) owns all legal right, title and interest in and to the Art, and all intellectual property rights therein. The rights that you have in and to the Art are limited to those described in this License. MDCC reserves all rights in and to the Art not expressly granted to you in this License.
                    </SubText>
                    <SubTitle>License.</SubTitle>
                    <Content>General Use. </Content>
                    <SubText>
                    Subject to your continued compliance with the terms of this License, MDCC grants you a worldwide, non-exclusive, non-transferable, royalty-free license to use, copy, and display the Art for your Purchased NFTs, along with any Extensions that you choose to create or use, solely for the following purposes: (i) for your own personal, non-commercial use; (ii) as part of a marketplace that permits the purchase and sale of your NFTs, provided that the marketplace cryptographically verifies each NFT owner’s rights to display the Art for their Purchased NFTs to ensure that only the actual owner can display the Art; or (iii) as part of a third party website or application that permits the inclusion, involvement, or participation of your NFTs, provided that the website/application cryptographically verifies each NFT owner’s rights to display the Art for their Purchased NFTs to ensure that only the actual owner can display the Art, and provided that the Art is no longer visible once the owner of the Purchased NFT leaves the website/application.
                    </SubText>
                    <Content> Commercial Use.</Content>
                    <SubText>
                    Subject to your continued compliance with the terms of this License, MDCC grants you a limited, worldwide, non-exclusive, non-transferable license to use, copy, and display the Art for your Purchased NFTs for the purpose of commercializing your own merchandise that includes, contains, or consists of the Art for your Purchased NFTs (“Commercial Use”), provided that such Commercial Use does not result in you earning more than One Hundred Thousand Dollars ($100,000) in gross revenue each year. For the sake of clarity, nothing in this Section 3.b will be deemed to restrict you from (i) owning or operating a marketplace that permits the use and sale of NFTs generally, provided that the marketplace cryptographically verifies each NFT owner’s rights to display the Art for their Purchased NFTs to ensure that only the actual owner can display the Art; (ii) owning or operating a third party website or application that permits the inclusion, involvement, or participation of NFTs generally, provided that the third party website or application cryptographically verifies each NFT owner’s rights to display the Art for their Purchased NFTs to ensure that only the actual owner can display the Art, and provided that the Art is no longer visible once the owner of the Purchased NFT leaves the website/application; or (iii) earning revenue from any of the foregoing, even where such revenue is in excess of $100,000 per year.
                    </SubText>
                    <Content> Restrictions.</Content>
                    <SubText>
                    You agree that you may not, nor permit any third party to do or attempt to do any of the foregoing without MDCC’s express prior written consent in each case: (i) modify the Art for your Purchased NFT in any way, including, without limitation, the shapes, designs, drawings, attributes, or color schemes (your use of Extensions will not constitute a prohibited modification hereunder); (ii) use the Art for your Purchased NFTs to advertise, market, or sell any third party product or service; (iii) use the Art for your Purchased NFTs in connection with images, videos, or other forms of media that depict hatred, intolerance, violence, cruelty, or anything else that could reasonably be found to constitute hate speech or otherwise infringe upon the rights of others; (iv) use the Art for your Purchased NFTs in movies, videos, or any other forms of media, except to the limited extent that such use is expressly permitted in Section “Commercial Use” above or solely for your own personal, non-commercial use; (v) sell, distribute for commercial gain (including, without limitation, giving away in the hopes of eventual commercial gain), or otherwise commercialize merchandise that includes, contains, or consists of the Art for your Purchased NFTs, except as expressly permitted in Section 3(b) above; (vi) attempt to trademark, copyright, or otherwise acquire additional intellectual property rights in or to the Art for your Purchased NFTs; or (vii) otherwise utilize the Art for your Purchased NFTs for your or any third party’s commercial benefit. To the extent that Art associated with your Purchased NFTs contains Third Party IP (e.g., licensed intellectual property from a celebrity, athlete, or other public figure), you understand and agree as follows: (w) that you will not have the right to use such Third Party IP in any way except as incorporated in the Art, and subject to the license and restrictions contained herein; (x) that the Commercial Use license in Section “Commercial Use” above will not apply; (y) that, depending on the nature of the license granted from the owner of the Third Party IP, MDCC may need to pass through additional restrictions on your ability to use the Art; and (z) to the extent that MDCC informs you of such additional restrictions in writing (email is permissible), you will be responsible for complying with all such restrictions from the date that you receive the notice, and that failure to do so will be deemed a breach of this license. The restriction in Section 4 will survive the expiration or termination of this License.
                    </SubText>
                    <Content> Terms of License.</Content>
                    <SubText>
                    The license granted in Section “License” above applies only to the extent that you continue to Own the applicable Purchased NFT. If at any time you sell, trade, donate, give away, transfer, or otherwise dispose of your Purchased NFT for any reason, the license granted in Section “License” will immediately expire with respect to those NFTs without the requirement of notice, and you will have no further rights in or to the Art for those NFTs. If you exceed the $100,000 limitation on annual gross revenue set forth in Section “Commercial Use” above, you will be in breach of this License, and must send an email to MDCC at <b> legal@maddogcarclub.io</b> within fifteen (15) days, with the phrase “NFT License - Commercial Use” in the subject line, requesting a discussion with MDCC regarding entering into a broader license agreement or obtaining an exemption (which may be granted or withheld in MDCC’s sole and absolute discretion). If you exceed the scope of the license grant in Section “Commercial Use” without entering into a broader license agreement with or obtaining an exemption from MDCC, you acknowledge and agree that: (i) you are in breach of this License; (ii) in addition to any remedies that may be available to MDCC at law or in equity, the MDCC may immediately terminate this License, without the requirement of notice; and (iii) you will be responsible to reimburse MDCC for any costs and expenses incurred by MDCC during the course of enforcing the terms of this License against you.
                    </SubText>
                    <SubTitle>Accounts and E-commerce</SubTitle>
                    <Content>Account Creation and Termination </Content>
                    <SubText>
                    In order to use certain features of the Platform, you may be required to register for an account (“Account”) by providing certain information about yourself as prompted by our registration form. You represent and warrant that: (a) all information you provide is truthful and accurate; and (b) you will maintain the accuracy of such information in your Account at all times. Your registration information must not misrepresent your identity or your affiliation with any person or organization. You may request changes to your registration information and/or terminate your Account at any time, for any reason, by sending a written e-mail request with “Update Information” and/or “Cancel Account” (as applicable) in the subject line to [insert link to email address].  Please allow at least 72 hours for updates and/or terminations to take effect. Without limiting anything set out elsewhere in this Agreement, we may, at our discretion, suspend or terminate your Account at any time for any reason without affecting our rights under this Agreement.
                    </SubText>
                    <Content>Account Responsibilities </Content>
                    <SubText>
                    You are fully responsible for maintaining the confidentiality of your Account login information and for all activities that occur under your Account, including the use of your Account by members of your household or anyone else. You agree to immediately notify us of any unauthorized use, or suspected unauthorized use of your Account, or any other breach of security of your Account, by sending an e-mail to [insert link to email address] with “Unauthorized Use” in the subject line. We are not liable for any loss or damage arising from your failure to restrict access to your Account.
                    </SubText>
                    <Content>E-commerce </Content>
                    <SubText>
                    All purchases of physical items are made pursuant to a shipment contract, wherein the risk of loss and title for such items pass to you upon our delivery to the carrier.
                    </SubText>
                    <Content>Security</Content>
                    <SubText>
                    From time to time, we may use techniques designed to identify fraudulent activities. You agree to cooperate with any efforts as we may, in our sole discretion, deem necessary to identify attempted fraud. You further agree that if, for any reason, you or others acting on your behalf are suspected of fraud or other violations of this Agreement, we may, in our sole discretion, declare you to be in breach of this Agreement, suspend, block and/or terminate your use of the Platform, and/or seek prosecution to the fullest extent of the law.
                    </SubText>
                    <Content>Indemnity</Content>
                    <SubText>
                    You agree to indemnify, defend and hold harmless MDCC, its agents, suppliers, licensees, content providers, successors and assigns, and its and their respective officers, directors, employees, contractors and agents (collectively, “Indemnified Parties”) from and against any and all damages, harm, loss, liability, claims, actions, demands, costs and expenses, including reasonable attorneys' fees and costs of settlement, arising out of or related to (i) your use of the Platform, the Materials, or any element or component thereof, (ii) your User Content, and/or (iii) your violation of this Agreement or any applicable laws, rules or regulations. We reserve the right, at your expense, to assume the exclusive defense and control of any matter for which you are required to indemnify us and you agree to cooperate with our defense of these claims. You further agree not to settle any matter without our prior written consent. We will use reasonable efforts to notify you of any such claim, action or proceeding upon becoming aware of it. This Section shall survive expiration or termination of this Agreement.
                    </SubText>
                    <Content>Release</Content>
                    <SubText>
                    Without limiting anything in this Agreement to the contrary, you hereby release and forever discharge each of the Indemnified Parties from any and all damages, harm, loss, liability, claims, actions, demands, costs and expenses of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, past, present or future (including personal injuries, death, and property damage), that has arisen or arises directly or indirectly out of, or relates directly or indirectly to, this Agreement, our [insert link to privacy notice], and/or any use or access by you of the Platform, including any interactions with, or acts or omissions of, retailers or other operators of Third Party Sites. IF YOU ARE A CALIFORNIA RESIDENT, YOU HEREBY WAIVE CALIFORNIA CIVIL CODE SECTION 1542 IN CONNECTION WITH THE FOREGOING, WHICH STATES: "A GENERAL RELEASE DOES NOT EXTEND TO CLAIMS WHICH THE CREDITOR DOES NOT KNOW OR SUSPECT TO EXIST IN HIS OR HER FAVOR AT THE TIME OF EXECUTING THE RELEASE, WHICH IF KNOWN BY HIM OR HER MUST HAVE MATERIALLY AFFECTED HIS OR HER SETTLEMENT WITH THE DEBTOR." This Section shall survive expiration or termination of this Agreement.
                    </SubText>
                    <Content>Disclaimers</Content>
                    <SubText>
                    THE PLATFORM IS PROVIDED ON AN "AS-IS" AND "AS AVAILABLE" BASIS WITHOUT WARRANTIES OF ANY KIND. TO THE FULLEST EXTENT PERMITTED BY LAW, EACH OF THE INDEMNIFIED PARTIES EXPRESSLY DISCLAIMS ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING THE WARRANTIES OF MERCHANTABILITY, NON-INFRINGEMENT OF THIRD-PARTY RIGHTS, AND FITNESS FOR A PARTICULAR PURPOSE, AND MAKES NO WARRANTIES ABOUT THE ACCURACY, RELIABILITY, COMPLETENESS, INTENT, USEFULNESS OR TIMELINESS OF THE PLATFORM AND/OR THE MATERIAL. FOR THE AVOIDANCE OF DOUBT, EACH OF THE INDEMNIFIED PARTIES MAKES NO REPRESENTATION OR WARRANTY THAT: (A) THE PLATFORM WILL MEET YOUR REQUIREMENTS; (B) THE PLATFORM WILL BE AVAILABLE ON AN UNINTERRUPTED, TIMELY, SECURE OR ERROR-FREE BASIS; (C) THE QUALITY OF THE MATERIAL WILL MEET YOUR EXPECTATIONS; (D) ANY ERRORS IN THE PLATFORM WILL BE CORRECTED; (E) THE PLATFORM WILL BE ACCURATE, RELIABLE, FREE OF VIRUSES OR OTHER HARMFUL CODE, COMPLETE, LEGAL OR SAFE; OR (E) ANY OF THE PLATFORM OR MATERIALS IS APPROPRIATE OR MAY BE DOWNLOADED OUTSIDE OF THE UNITED STATES. IF YOUR USE OF THE PLATFORM OR ANY MATERIALS RESULTS IN THE NEED FOR SERVICE TO, OR REPLACEMENT OF, EQUIPMENT OR DATA, MDCC IS NEITHER RESPONSIBLE NOR LIABLE FOR THOSE COSTS. YOUR USE AND BROWSING OF, AND ANY RELIANCE BY YOU UPON, THE PLATFORM, ARE AT YOUR OWN RISK. IF YOU ARE DISSATISFIED WITH THE PLATFORM OR ANY MATERIALS, OR WITH ANY OF THE TERMS AND CONDITIONS OF THIS AGREEMENT, YOUR SOLE AND EXCLUSIVE REMEDY IS TO DISCONTINUE ACCESSING AND USING THE PLATFORM. THIS SECTION SHALL SURVIVE EXPIRATION OR TERMINATION OF THIS AGREEMENT.
                    </SubText>
                    <Content>Limitation of Liability</Content>
                    <SubText>
                    TO THE FULLEST EXTENT PERMISSIBLE UNDER APPLICABLE LAW, IN NO EVENT SHALL MDCC, OR ANY OF THE OTHER INDEMNIFIED PARTIES, BE LIABLE TO YOU OR ANY THIRD PARTY FOR ANY DIRECT, INDIRECT, CONSEQUENTIAL, EXEMPLARY, INCIDENTAL, SPECIAL, PUNITIVE OR SIMILAR DAMAGES ARISING FROM OR RELATING TO THIS AGREEMENT OR YOUR USE OF OR ACCESS TO, OR INABILITY TO USE OR ACCESS, THE PLATFORM, INCLUDING DAMAGES FOR HARM TO BUSINESS, LOST PROFITS, LOST SAVINGS OR LOST REVENUES, HOWEVER SUCH DAMAGES ARE CAUSED AND WHETHER BASED IN CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER THEORY OF LIABILITY. THE FOREGOING LIMITATIONS SHALL APPLY REGARDLESS OF WHETHER ANY SUCH INDEMNIFIED PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH INJURY, DAMAGES, LOSSES OR EXPENSES. MDCC SHALL NOT BE LIABLE FOR ANY OF YOUR INTERACTIONS OR TRANSACTIONS WITH THIRD PARTY SITES, INCLUDING RETAILERS. THIS SECTION SHALL SURVIVE EXPIRATION OR TERMINATION OF THIS AGREEMENT.
                    </SubText>
                    <SubText>
                    ACCESS TO, AND USE OF, THE PLATFORM ARE AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR COMPUTER SYSTEM OR LOSS OF DATA RESULTING THEREFROM. IN THE EVENT YOU TRANSMIT, INTRODUCE OR OTHERWISE CAUSE ANY TECHNICAL DISRUPTION OF THE PLATFORM OR THE NETWORKS, SYSTEMS OR EQUIPMENT SUPPORTING THE PLATFORM, YOU AGREE TO BE RESPONSIBLE FOR ANY AND ALL LIABILITIES AND COSTS AND EXPENSES (INCLUDING ATTORNEYS' FEES AND EXPENSES) ARISING FROM ANY AND ALL CLAIMS BROUGHT BY THIRD PARTIES BASED UPON SUCH TECHNICAL DISRUPTIONS. "TECHNICAL DISRUPTIONS" INCLUDE DISTRIBUTION OF UNSOLICITED ADVERTISING OR CHAIN LETTERS, PROPAGATION OF COMPUTER WORMS, VIRUSES OR OTHER HARMFUL CODE, AND/OR USING THE PLATFORM TO MAKE UNAUTHORIZED ENTRY TO ANY OTHER MACHINE ACCESSIBLE VIA THE PLATFORM. YOU ARE FURTHER SOLELY RESPONSIBLE FOR THE CONTENT OF ANY TRANSMISSIONS USING THE PLATFORM AND, WITHOUT LIMITING ANYTHING SET OUT IN THIS AGREEMENT, YOU AGREE NOT TO UPLOAD, POST OR OTHERWISE MAKE AVAILABLE ON THE PLATFORM ANY MATERIAL PROTECTED BY A PROPRIETARY RIGHT OF A THIRD PARTY WITHOUT FIRST OBTAINING THE EXPRESS PERMISSION OF THE OWNER OF SUCH PROPRIETARY RIGHT. YOU SHALL BE SOLELY LIABLE FOR ANY DAMAGES, LOSSES, COSTS AND EXPENSES (INCLUDING ATTORNEYS' FEES AND EXPENSES) ARISING OUT OF INFRINGEMENT OF PROPRIETARY RIGHTS OR ANY OTHER HARM ARISING FROM THE UPLOADING, POSTING OR OTHER SUBMISSION OF MATERIALS BY YOU, INCLUDING USER CONTENT. THIS SECTION SHALL SURVIVE EXPIRATION OR TERMINATION OF THIS AGREEMENT.
                    </SubText>
                    <SubText>
                    IF AN INDEMNIFIED PARTY IS FOUND TO HAVE LIABILITY IN CONNECTION WITH THIS AGREEMENT, THEN NOTWITHSTANDING ANYTHING TO THE CONTRARY CONTAINED HEREIN THAT MAY SUGGEST OTHERWISE, THE AGGREGATE LIABILITY OF THE INDEMNIFIED PARTIES TO YOU FOR ANY AND ALL DAMAGES ARISING FROM OR RELATED TO THIS AGREEMENT (FOR ANY CAUSE WHATSOEVER AND REGARDLESS OF THE FORM OF THE ACTION), WILL AT ALL TIMES BE LIMITED TO FIFTY US DOLLARS ($50) AND THE EXISTENCE OF MORE THAN ONE CLAIM WILL NOT INCREASE THIS LIMIT. SOME JURISDICTIONS DO NOT ALLOW THE LIMITATION OR EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU. YOU MAY ALSO HAVE OTHER LEGAL RIGHTS THAT VARY FROM JURISDICTION TO JURISDICTION.
                    </SubText>
                    <Content>Term and Termination</Content>
                    <SubText>
                    Except as otherwise set forth herein, this Agreement will remain in full force and effect while you use and access the Platform. We reserve the right to (a) suspend or terminate your rights to use the Platform (including your Account), or (b) terminate this Agreement, at any time for any reason, in our sole discretion, including for any use of, or access to, the Platform in violation of this Agreement. Upon termination of this Agreement, your Account and right to access and use the Platform will terminate immediately. You understand that any termination of your Account may involve deletion of your User Content associated therewith from our live databases. We will not have any liability whatsoever to you for any termination of this Agreement, including for termination of your Account or deletion of your User Content. Our rights under this Agreement will survive any termination of your Account or this Agreement.
                    </SubText>
                    <SubTitle>General</SubTitle>
                    <Content>No Support or Maintenance </Content>
                    <SubText>
                    You acknowledge and agree that we will have no obligation to provide you with any support or maintenance in connection with the Platform.
                    </SubText>
                    <Content>International Access </Content>
                    <SubText>
                    Access to the Platform may not be legal by certain persons, or in certain countries. This Agreement will not be governed by the United Nations Convention on Contracts for the International Sale of Goods. You will comply with all applicable export laws, restrictions, and regulations of any United States or foreign agency or authority and will not export or re-export, or allow the export or re-export of any product, material, service, technology or information you obtain or acquire in connection with the Platform (or any direct product thereof) in violation of any such laws, restrictions or regulations. While you may be able to setup an Account as a user outside the U.S., we make no assurances or representations of any kind that the Platform is suitable for use in the country in which you reside. Currently, users outside the U.S. may not purchase merchandise through the Platform. If you access the Platform from outside of the United States, you do so at your own risk and are responsible for compliance with the laws of the jurisdiction in which you are then-located, and will indemnify and hold harmless MDCC and all other Indemnified Parties from and against any and all damages, harm, loss, liability, claims, actions, demands, costs and expenses, including reasonable attorneys' fees and costs of settlement, arising out of or related to such access.
                    </SubText>
                    <Content>Governing Law; Jurisdiction</Content>
                    <SubText>
                    The Platform and these terms and conditions are governed by the internal substantive laws of the State of California without regard to conflict of law provisions. Jurisdiction for any claims arising under this Agreement shall lie exclusively with the state or federal courts in San Diego, CA. To the fullest extent permitted by applicable law, no claim under this Agreement may be joined to any other claim, including any legal proceeding involving any other current or former user of the Platform, and no class action proceedings will be permitted. You agree that regardless of any statute or law to the contrary, any claim or cause of action arising out of or related to use of the Platform or this Agreement must be filed within one (1) year after such claim or cause of action arose. This Agreement and any notice given in electronic form shall be admissible in judicial or administrative proceedings based upon or relating to this Agreement to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form. This Section will survive termination of this Agreement.
                    </SubText>
                    <Content>Entire Agreement; Waiver and Severability; Interpretation; Assignment</Content>
                    <SubText>
                    This Agreement, which include any specific terms appearing at discrete areas of the Platform, constitutes the entire agreement between you and MDCC regarding the Platform. Our failure to exercise or enforce any right or provision of this Agreement shall not operate as a waiver of such right or provision. If any provision of this Agreement is, for any reason, held to be invalid or unenforceable, the other provisions of this Agreement will be unimpaired and the invalid or unenforceable provision will be deemed modified so that it is valid and enforceable to the maximum extent permitted by law. The section titles in this Agreement are for convenience only and have no legal or contractual effect. The use of the word “including” in this Agreement shall mean “including without limitation.” This Agreement, and your rights and obligations herein, may not be assigned, subcontracted, delegated, or otherwise transferred by you without our prior written consent, and any attempted assignment, subcontract, delegation, or transfer in violation of the foregoing will be null and void. The terms of this Agreement shall be binding upon permitted assignees.
                    </SubText>
                    <Content>Export and Sanctions</Content>
                    <SubText>
                    You may not use Platform if you are subject to U.S. sanctions or sanctions consistent with U.S. law imposed by the governments of the country where you are using the Platform. You must comply with all U.S. or other export and re-export restrictions that may apply to goods, software, technology and services.
                    </SubText>
                    <Content>Consumer Complaints</Content>
                    <SubText>
                    In accordance with California Civil Code §1789.3, you may report complaints to the Complaint Assistance Unit of the Division of Consumer Services of the California Department of Consumer Affairs by contacting them in writing at 400 R Street, Sacramento, CA 95814, or by telephone at (800) 952-5210.
                    </SubText>
                </Box>
                </Container>
            </Section>  
            <FooterSection>
            <ScrollToTop />
                <FooterContainer>
                    <Box>
                        <FooterBox>
                            <FooterTitle>
                            © MAD DOG CAR CLUB<br/>
                            ALL RIGHTS RESERVED<br/>
                            <Link to='/terms'>TERMS</Link> &#38; <Link to='/policy'>POLICY</Link> 
                            </FooterTitle>
                            <SocialIcon />
                        </FooterBox>
                    </Box>
                </FooterContainer>
            </FooterSection>
        </ThemeProvider>      
        </>
    )
}

export default Terms