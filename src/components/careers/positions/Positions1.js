import React, { useEffect } from "react";
import styled from "styled-components";
import { ThemeProvider } from "styled-components";
import Navigation from "../../../functions/Navigation2";
import ScrollToTop from "../../../functions/ScrollToTop";
import SocialIcon from "../../../functions/SocialIcon";
import GlobalStyles from "../../../styles/GlobalStyles";
import { light } from "../../../styles/Themes";
import FooterBackground from '../../../assets/img/Background.jpg';
import HeaderImage from "../../../assets/img/position.jpg";
import { Link } from "react-router-dom";
import Arrow2 from "../../../Icons/Arrow2";

// import DesignerBrandForm from "../../../forms/DesignerBrandForm";

const Section = styled.section`
min-height: 100%;
width: 100%;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;

iframe{
    width: 102%;
    height: 100%;
    min-height: 122vh;
}

@media (max-width: 428px){
    min-height: 450px;
}
`

const FooterContainer = styled.div`
width: 75%;
min-height: 20vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;
`

const FooterBox = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

div{
    margin-top: 20px;
}

div a img{
    width: 35px;
}

@media (max-width: 428px){
    margin-top: 25px;
}
`

const FooterTitle = styled.p`
font-size: ${props => props.theme.fontmd};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: center;

a:hover{
    color:#85FF10;
  }
`

const FooterSection = styled.section`
width: 100%;
padding: 50px 0;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;
background-image: url(${FooterBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;

@media (max-width: 428px){
    padding: 0;
}
`

const Container = styled.div`
width: 75%;
min-height: 350px;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: start;
margin-top: 0;
margin-bottom: 0;

@media (max-width: 428px){
    display: block;
    width: 100%;
    padding: 0 20px;
}
`

const Box = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
margin-top: 50px;
margin-bottom: 50px;

@media (max-width: 428px){
    width: 100%;
}
`

const BoxOne = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-start;
margin-top: 50px;
margin-bottom: 50px;
`

const BoxTwo = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
margin-top: 50px;
margin-bottom: 50px;
`

const HeaderCareerImage = styled.div`
margin: 0;
padding: 0;
width: 100%;
height: 100%;
background-image: url(${HeaderImage});
background-size: cover;
background-repeat: no-repeat;
padding: 55px 142px;
margin-top: 158px;

@media (max-width: 428px){
    padding: 20px;
    margin-top: 95px;
}
`

const HeaderTitle = styled.h2`
font-size: ${(props) => props.theme.fontxxl};
text-transform: uppercase;
text-align: left;
color: ${(props) => props.theme.body};
font-family: "Floyd", sans-serif;

@media (max-width: 428px){
    font-size: 1.5em;
}
`

const SubTitle = styled.p`
font-size: ${(props) => props.theme.fontlg};
text-transform: uppercase;
text-align: left;
color: ${(props) => props.theme.body};
`

const TitleCareer = styled.h2`
font-size: ${(props) => props.theme.fontxl};
text-transform: uppercase;
text-align: center;
color: ${(props) => props.theme.text};
font-family: "Barlow Condensed Bold", sans-serif;
`

const SubCareer = styled.h5`
font-size: ${(props) => props.theme.fontmd};
text-align: left;
color: ${(props) => props.theme.text};
p > b{
    font-family: "Barlow Condensed Bold", sans-serif;
    text-transform: uppercase;
}
`

const Breadcrumb = styled.div`
    width: 152px;    
    font-size: ${(props) => props.theme.fontlg};
    text-transform: uppercase;
    display: flex;
    color: #fff;
    padding: 10px 0;
    border-bottom: 1.5px solid #fff;
    margin-bottom: 20px;
    svg{
        width: 28px;
        height: auto;
        margin-right: 10px;
    }
`

const ContactFormContainer = styled.div`
padding: 50px;
background-color: #E5E5E5;
border: 1px solid #000;
width:100%;

h4 {    
    font-size: ${(props) => props.theme.fontlx};
text-transform: uppercase;
font-family: "Barlow Condensed Bold", sans-serif;
}

form div{
    padding: 5px 0;
}

form div > input, form div > textarea{
color: #000;
font-size: ${(props) => props.theme.fontmd};
text-transform: uppercase;
width: 100%;
border: none;
border-bottom: 2px solid #fff;
background-color: transparent;
padding: 5px 0;
}

form div > label{
    color: #000;
    font-size: ${(props) => props.theme.fontlg};
    font-family: "Barlow Condensed Bold", sans-serif;
    text-transform: uppercase;
}

@media (max-width: 428px){
    width: 100%;
    padding: 30px 15px;
}
`

const BoxForm = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
padding: 50px 25px 50px;

@media (max-width: 428px){
    width: 100%;
    padding: 20px 10px;
    margin-bottom: -450px;
}
`

const Position1 = () => {
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    return(
        <>
        <sub-section id="home"></sub-section>
        <GlobalStyles />
        
        <ThemeProvider theme={light}>
            <Navigation />
            <HeaderCareerImage>
            <Link to='/careers'><Breadcrumb><Arrow2 /> Open Positions</Breadcrumb></Link>
                <HeaderTitle>Designer, Branding</HeaderTitle>
                <SubTitle>Part-time // Remote</SubTitle>
            </HeaderCareerImage>
            <Section>
                <Container>
                <Box>
                   <SubCareer>
                   <p><b>Description:</b></p>
                   <p>Mad Dog Car Club is a club for dog and car lovers alike, with a welcoming community and a destination of sustainability through the symbiosis of our NFT collections, MD token, and P2E Game.</p> 

                   <p><b>The Role:</b></p> 
                    <p></p>As a Designer at MDCC, you will work on creating MDCC branded content across digital, physical and marketing initiatives. We are looking for a well-rounded designer, experienced in different mediums, across print and digital media. In this role, you will be working with our Chief of Design on MDCC related projects. This role is remote, preferably located in the US and an opportunity to become a full-time position. 
                    <ul>
                        <li>Work collaboratively with our Chief of Design and marketing department at MDCC </li>
                        <li>Be able to work on handle a variety of creative projects in a fast-paced environment</li>
                        <li>Work within MDCC’s established brand guidelines </li>
                        <li>Create marketing assets for both digital and print</li>
                        <li>Develop design concepts and content solutions </li>
                    </ul> 

                     <p><b>Requirements:</b></p> 
                    <ul>
                        <li>A portfolio that showcases your creativity and design skills </li>
                        <li>Experience in marketing/advertising for at least one year</li>
                        <li>Familiarity across various social media platforms</li>
                        <li>Have worked in both digital and print</li>
                        <li>Strong communication skills and ability to incorporate feedback and take/give clear direction</li>
                        <li>Impeccable organization skills </li>
                        <li>Proficiency in Adobe products (Working in Figma and After Effects is a plus)</li>
                        <li>Experience in motion graphics and illustration is a plus </li>
                    </ul> 
                    
                    <p><b>Why You’ll Love The MDCC Team: </b></p> 
                    <ul>
                        <li>You will play a key role in developing the future of MDCC as a brand </li>
                        <li>Opportunity to work with a team of passionate, forward thinking individuals in the web3 space </li>
                        <li>We offer flexible work hours and salary-based pay for full time employees</li>
                    </ul>

                   </SubCareer>
                </Box>
                <BoxForm>
                {/* <ContactFormContainer>
                    <h4>APPLICATION FORM</h4>
                  <DesignerBrandForm />
                </ContactFormContainer> */}
                <iframe src="https://maddogcarclub.io/forms/form1/" title="MDCC form"/>
                </BoxForm>
                </Container>
            </Section>  
            <FooterSection>
            <ScrollToTop />
                <FooterContainer>
                    <Box>
                        <FooterBox>
                            <FooterTitle>
                            © MAD DOG CAR CLUB<br/>
                            ALL RIGHTS RESERVED<br/>
                            <Link to='/terms'>TERMS</Link> &#38; <Link to='/policy'>POLICY</Link> 
                            </FooterTitle>
                            <SocialIcon />
                        </FooterBox>
                    </Box>
                </FooterContainer>
            </FooterSection>
        </ThemeProvider>
        </>
    )
}

export default Position1