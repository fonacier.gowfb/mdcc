import React, { useEffect } from "react";
import styled from "styled-components";
import { ThemeProvider } from "styled-components";
import Navigation from "../../functions/Navigation2";
import ScrollToTop from "../../functions/ScrollToTop";
import SocialIcon from "../../functions/SocialIcon";
import GlobalStyles from "../../styles/GlobalStyles";
import { dark, light } from "../../styles/Themes";
import FooterBackground from '../../assets/img/Background.jpg';
import HeaderImage from "../../assets/img/careers-header.jpg";
import Button from "../../functions/Button";
// import Designer from "./positions/Positions1";
import { Link } from "react-router-dom";

const Section = styled.section`
min-height: 80vh;
width: 100%;
background-color: ${props => props.theme.body};
display: flex;
flex-direction: column;
justify-content: flex-start;
align-items: center;
position: relative;

div.button > a{
    display: inline-block;
    background-color: #85FF10;
    color: #202020;
    outline: none;
    border: none;
    font-size: 1.25em;
    font-family: "Barlow Condensed Bold",sans serif;
    padding: 0.9rem 2.3rem;
    border-radius: 0;
    cursor: pointer;
    -webkit-transition: all 0.2s ease;
    transition: all 0.2s ease;
    position: relative;
}

@media (max-width: 428px){
    min-height: 750px;
}
`

const FooterContainer = styled.div`
width: 75%;
min-height: 20vh;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: center;
`

const FooterBox = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;

div{
    margin-top: 20px;
}

div a img{
    width: 35px;
}

@media (max-width: 428px){
    margin-top: 25px;
}
`

const FooterTitle = styled.p`
font-size: ${props => props.theme.fontmd};
text-transform: uppercase;
color: ${props => props.theme.body};
align-self: flex-start;
width: 100%;
margin: 0 auto;
text-align: center;

a:hover{
    color:#85FF10;
  }
`

const FooterSection = styled.section`
width: 100%;
padding: 50px 0;
background-color: ${props => props.theme.body};
display: flex;
justify-content: center;
align-items: center;
position: relative;
background-image: url(${FooterBackground});
background-repeat: no-repeat;
background-size: cover;
background-position: center center;

@media (max-width: 428px){
    padding: 0;
}
`

const Container = styled.div`
width: 75%;
min-height: 100px;
margin: 0 auto;
/*background-color: lightblue;*/

display: flex;
justify-content: center;
align-items: start;
margin-top: 0;
margin-bottom: 0;
border-bottom: 2px solid #C4C4C4;

@media (max-width: 428px){
    display: block;
    align-items: center;
}
`

const Box = styled.div`
width: 85%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
margin-top: 50px;
margin-bottom: 50px;
`

const BoxOne = styled.div`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-start;
margin-top: 50px;
margin-bottom: 50px;
`

const BoxTwo = styled.div`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
justify-content: center;
align-items: flex-end;
margin-top: 50px;
margin-bottom: 50px;

a{
    font-family: "Barlow Condensed Bold", sans-serif;
}

@media (max-width: 428px){
    align-items: flex-start;
}
`

const HeaderCareerImage = styled.div`
margin: 0;
padding: 0;
width: 100%;
height: 50vh;
background-image: url(${HeaderImage});
background-size: cover;
background-repeat: no-repeat;
padding: 55px 0;
margin-top: 158px;

@media (max-width: 428px){
    height: 100%;
    padding: 9px 0 82px;
    margin-top: 92px;
}

@media (min-width: 429px){
    height: 100%;
    padding: 20px 0 170px;
    margin-top: 92px;
}

@media (min-width: 748px){
    height: 100%;
    padding: 26px 0 120px;
    margin-top: 93px;
}

@media (min-width: 914px){
    height: 100%;
    padding: 26px 0 195px;
    margin-top: 159px;
}

@media (min-width: 928px){
    height: 100%;
    padding: 40px 0 169px;
}

@media (min-width: 928px){
    height: 100%;
    padding: 40px 0 169px;
}

@media (min-width: 1024px){
    height: 100%;
    padding: 54px 0 234px;
}

@media (min-width: 1200px){
    height: 100%;
    padding: 80px 0 240px;
}

@media (min-width: 1400px){
    height: 100%;
    padding: 80px 0 325px;
}
`

const HeaderTitle = styled.h2`
font-size: ${(props) => props.theme.fontxxxl};
text-transform: uppercase;
text-align: center;
color: ${(props) => props.theme.body};
font-family: "Floyd", sans-serif;

@media (max-width: 428px){
    font-size: ${(props) => props.theme.fontxl};
}
`

const TitleCareer = styled.h2`
font-size: ${(props) => props.theme.fontxxl};
text-transform: uppercase;
text-align: left;
color: ${(props) => props.theme.text};
font-family: "Barlow Condensed Bold", sans-serif;
line-height: 1em;

@media (max-width: 428px){
    font-size: 2.2em;
}
`

const SubCareer = styled.h5`
font-size: ${(props) => props.theme.fontlx};
font-family: "Barlow Condensed Medium", sans-serif;
text-align: left;
color: ${(props) => props.theme.text};
`

const Careers = () => {
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
    return(
        <>
        <GlobalStyles />
        <Section id="home">
        <ThemeProvider theme={light}>
            <Navigation />
            <HeaderCareerImage>
                <HeaderTitle>Careers</HeaderTitle>
            </HeaderCareerImage>
            <Section>
                <Container>
                <BoxOne>
                   <TitleCareer>Designer Branding</TitleCareer> 
                   <SubCareer>Part-time // Remote</SubCareer>
                </BoxOne>
                <BoxTwo>
                <div className="button">
                <ThemeProvider theme={dark}>
                    <Link to="/careers/positions/position1" >APPLY HERE</Link>
                </ThemeProvider>
                </div>
                </BoxTwo>
                </Container>
                <Container>
                <BoxOne>
                   <TitleCareer>Motion Designer, Branding</TitleCareer> 
                   <SubCareer>Part-time // Remote</SubCareer>
                </BoxOne>
                <BoxTwo>
                <div className="button">
                <ThemeProvider theme={dark}>
                    <Link to="/careers/positions/position2" >APPLY HERE</Link>
                </ThemeProvider>
                </div>
                </BoxTwo>
                </Container>
                <Container>
                <BoxOne>
                   <TitleCareer>Blockchain Developer</TitleCareer> 
                   <SubCareer>Full-time // Remote</SubCareer>
                </BoxOne>
                <BoxTwo>
                <div className="button">
                <ThemeProvider theme={dark}> 
                    <Link to="/careers/positions/position3" >APPLY HERE</Link>
                </ThemeProvider>
                </div>
                </BoxTwo>
                </Container>
            </Section>  
            <FooterSection>
            <ScrollToTop />
                <FooterContainer>
                    <Box>
                        <FooterBox>
                            <FooterTitle>
                            © MAD DOG CAR CLUB<br/>
                            ALL RIGHTS RESERVED<br/>
                            <Link to='/terms'>TERMS</Link> &#38; <Link to='/policy'>POLICY</Link> 
                            </FooterTitle>
                            <SocialIcon />
                        </FooterBox>
                    </Box>
                </FooterContainer>
            </FooterSection>
        </ThemeProvider>
        </Section>
        </>
    )
}
export default Careers