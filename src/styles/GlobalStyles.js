import { createGlobalStyle } from "styled-components";
import "@fontsource/akaya-telivigala";
import "@fontsource/sora";
import "@fontsource/barlow-condensed";

const GlobalStyles = createGlobalStyle`
*,*::before,*::after{
    margin:0;
    padding:0;
}

body{
    font-family: "Barlow Condensed", sans-serif;
    overflow-x: hidden;
    background-color:#000;
}

h1,h2,h3,h4,h5,h6{
    margin:0;
    padding:0;
}

a{
    color:inherit;
    text-decoration:none;
    font-weight: 600;
}
`


export default GlobalStyles;