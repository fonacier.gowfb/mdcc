import * as React from "react"

const Arrow2 = (props) => (
  <svg
    id="a"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 36.77 14.97"
    {...props}
  >
    <defs>
      <style>
        {
          ".b{fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:3px}"
        }
      </style>
    </defs>
    <path className="b" d="M8.96 1.5 1.5 7.17l7.46 6.3M35.27 7.2H1.56" />
  </svg>
)

export default Arrow2