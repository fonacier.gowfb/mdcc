import * as React from "react";
// import DrawSVGPlugin from "gsap-trial/dist/DrawSVGPlugin";

const Vector = ({ progress = 0, ...props }) => {

  return (
    <>
    <svg
    id="a"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 74 3436.93"
    {...props}
  >
    <defs>
      <style>{".b{fill:none;stroke:#fff;stroke-width:3px}"}</style>
    </defs>
    <path className="b" d="M72.5 0v3436.93M63.5 0v3436.93M37.5 0v6" />
    <path
      style={{
        strokeDasharray: "0 0 12.02 12.02",
        fill: "none",
        stroke: "#fff",
        strokeWidth: 3,
      }}
      d="M37.5 18.02v3406.9"
    />
    <path className="b" d="M37.5 3430.93v6M11.5 0v3436.93M1.5 0v3436.93" />
  </svg>
</>
  );
};

export default Vector;
