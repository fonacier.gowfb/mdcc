import * as React from "react";
// import DrawSVGPlugin from "gsap-trial/dist/DrawSVGPlugin";

const Vector2 = ({ progress = 0, ...props }) => {

  return (
    <>
    <svg
    id="a"
    xmlns="http://www.w3.org/2000/svg"
    viewBox="0 0 83 9439.3"
    {...props}
  >
    <defs>
      <style>{".c{fill:none;stroke:#fff;stroke-width:3px}"}</style>
    </defs>
    <path className="c" d="M81.5 0v9439.3M71.36 0v9439.3M42.06 0v6" />
    <path
      style={{
        fill: "none",
        stroke: "#fff",
        strokeWidth: 3,
        strokeDasharray: "0 0 12.01 12.01",
      }}
      d="M42.06 18.01V9427.3"
    />
    <path className="c" d="M42.06 9433.3v6M12.77 0v9439.3M1.5 0v9439.3" />
  </svg>
</>
  );
};

export default Vector2;
