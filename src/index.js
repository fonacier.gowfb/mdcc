import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import App from './App';
import '../node_modules/normalize.css/normalize.css';
import { HashRouter as Router } from 'react-router-dom';
// import reportWebVitals from './reportWebVitals';
import './fonts/Floyd.ttf';
import { Routes } from 'react-router-dom';
import { Route } from 'react-router-dom';
import Rarity from './components/rarity/Rarity';
import Terms from './components/terms/Terms';
import Policy from './components/policy/Policy';
import Careers from './components/careers/Careers';
import Position1 from './components/careers/positions/Positions1';
import Position2 from './components/careers/positions/Positions2';
import Position3 from './components/careers/positions/Positions3';
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

ReactDOM.render(
  <React.StrictMode>
    <Router>
    <Routes>
      <Route path="/" element={ <App /> }></Route>
      <Route path="/rarity" element={ <Rarity /> }></Route>
      <Route path="/terms" element={ <Terms /> }></Route>
      <Route path="/policy" element={ <Policy /> }></Route>
      <Route path="/careers" element={ <Careers /> }></Route>
      <Route path="/careers/positions/position1" element={ <Position1 /> }></Route>
      <Route path="/careers/positions/position2" element={ <Position2 /> }></Route>
      <Route path="/careers/positions/position3" element={ <Position3 /> }></Route>
    </Routes>
    </Router>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
