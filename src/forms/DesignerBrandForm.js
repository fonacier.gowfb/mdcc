import React, { useState } from "react";
import ButtonForm from "../functions/ButtonForm";
import { ThemeProvider } from "styled-components";
import { dark } from "../styles/Themes";
import styled from "styled-components";

const ButtonWrapper = styled.div`
button{
    width: 100%;
    text-align: center;
    margin-top: 40px;
    padding-top: 10px;
    padding-bottom: 10px; 
}
`

const DesignerBrandForm = () => {
  const [status, setStatus] = useState("Submit");
  const [submitted, setSubmitted] = useState(false);
  const handleSubmit = async (e) => {
    e.preventDefault();
    setStatus("Sending...");
    const { pos1, name, email, my_file, linkedin, portfolio } = e.target.elements;
    let details = {
      pos1: pos1.value,
      name: name.value,
      email: email.value,
      my_file: my_file.files[0].name,
      linkedin: linkedin.value,
      portfolio: portfolio.value,
    };
    let response = await fetch("https://maddogcarclub.io:5000/", {
      method: "POST",      
      mode: 'cors',
      headers: {
        "Content-Type": "application/json;charset=utf-8",
        "Access-Control-Allow-Origin":"*",
        "Access-Control-Allow-Headers":"*"
      },
      body: JSON.stringify(details),
    });
    setStatus("Submit");
    // let result = await response.json();
    // alert(result.status);

    setTimeout(() => {
      setSubmitted(true);
    }, 100);
  };

  if (submitted) {
    return (
      <>
        <h2>Thank you!</h2>
        <div>We'll be in touch soon.</div>
      </>
    );
  }
  return (
    <form onSubmit={handleSubmit}>
      <input type="hidden" id="pos1" value="DESIGNER, BRANDING" />
      <div>
        <label htmlFor="name">Full Name*</label>
        <input type="text" id="name" required />
      </div>
      <div>
        <label htmlFor="email">Email*</label>
        <input type="email" id="email" required />
      </div>
      <div>
        <label htmlFor="my_file">Resume*</label>
        <input type="file" id="my_file" required />
      </div>
      <div>
        <label htmlFor="name">Linkedin</label>
        <input type="text" id="linkedin" />
      </div>
      <div>
        <label htmlFor="name">Portfolio</label>
        <input type="text" id="portfolio" />
      </div>
      {/* <button type="submit">{status}</button> */}
      <ButtonWrapper>
      <ThemeProvider theme={dark}>
        <ButtonForm type="submit" text="SUBMIT APPLICATION" />
    </ThemeProvider>
      </ButtonWrapper>
    </form>
  );
};

export default DesignerBrandForm;