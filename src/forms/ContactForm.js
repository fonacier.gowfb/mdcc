import React, { useState } from "react";
import { ThemeProvider } from "styled-components";
import Button from "../functions/Button";
import { dark } from "../styles/Themes";
import styled from "styled-components";
import ButtonForm from "../functions/ButtonForm";

const FORM_ENDPOINT = ""; // TODO - fill on the later step

const ButtonWrapper = styled.div`
button{
    width: 100%;
    text-align: center;
    margin-top: 40px;
    padding-top: 10px;
    padding-bottom: 10px; 
}
`

const ContactForm = () => {
  const [submitted, setSubmitted] = useState(false);
  const handleSubmit = () => {
    setTimeout(() => {
      setSubmitted(true);
    }, 100);
  };

  if (submitted) {
    return (
      <>
        <h2>Thank you!</h2>
        <div>We'll be in touch soon.</div>
      </>
    );
  }

  return (
    <form
      action={FORM_ENDPOINT}
      onSubmit={handleSubmit}
      method="POST"
      target="_blank"
    >
      <div>
        <label>Full Name*</label>
        <input type="text" placeholder="" name="name" required />
      </div>
      
      <div>
        <label>Email*</label>
        <input type="email" placeholder="" name="email" required />
      </div>
      <div>
        <label>Resume*</label>
        <input type="file" placeholder="" name="my_file" required />
      </div>
      <div>
        <label>Linkedin(Optional)</label>
        <input type="text" placeholder="" name="linkedin"  />
      </div>
      <div>
        <label>Portfolio(Optional)</label>
        <input type="text" placeholder="" name="portfolio"  />
      </div>
      <ButtonWrapper>
      <ThemeProvider theme={dark}>
        <ButtonForm type="submit" text="SUBMIT APPLICATION" />
    </ThemeProvider>
      </ButtonWrapper>
    </form>
  );
};

export default ContactForm;