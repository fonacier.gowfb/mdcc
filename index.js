/* eslint no-console: 0*/
const express = require("express");
const router = express.Router();
const nodemailer = require("nodemailer");
const nodemailerSendgrid = require('nodemailer-sendgrid'); //require('../index'); 
const app = express();
app.use(express.json());
app.use((req,res, next)=>{
  res.setHeader('Access-Control-Allow-Origin',"https://maddogcarclub.io");
  res.setHeader('Access-Control-Allow-Headers',"*");
  res.header('Access-Control-Allow-Credentials', true);
  next();
});
app.use("/", router);
app.listen(5000, () => console.log("Server Running on port 5000"));

const transport = nodemailer.createTransport(
    nodemailerSendgrid({
        apiKey: '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'
    })
);

transport.verify((error) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Ready to Send");
    }
  });

  router.post("/", (req, res) => {

    // const fs = require("fs");

    // pathToAttachment = `${__dirname}/attachment.docx`;
    // attachment = fs.readFileSync(pathToAttachment).toString("base64");
    
    const pos1 = req.body.pos1;
    const name = req.body.name;
    const email = req.body.email;
    const linkedin = req.body.linkedin; 
    const portfolio = req.body.portfolio;
    const my_file = req.body.my_file;
    const mail = {
      from: "Mad Dog Car Club <willhelmet042002@gmail.com>",
      to: "willhelmet042002@gmail.com",
      subject: `Applicant for ${pos1}`,
      html: `<p>Name: ${name}</p>
             <p>Email: ${email}</p>
             <p>Linkedin: ${linkedin}</p>
             <p>Portfolio: ${portfolio}</p>`,
      attachments: [
               {
                filename: `${my_file}`,
                path: `${__dirname}/attachment.docx`,
               }
             ]
    };
    transport.sendMail(mail, (error) => {
      if (error) {
        res.json({ status: "ERROR" });
      } else {
        res.json({ status: "Message Sent" });
      }
    });
  });

// const sgMail = require('@sendgrid/mail');
//     sgMail.setApiKey('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');
//     const msg = {
//       from: "Mad Dog Car Club <willhelmet042002@gmail.com>",
//       to: "willhelmet042002@gmail.com",
//       subject: 'Sending with SendGrid is Fun',
//       text: 'and easy to do anywhere, even with Node.js',
//       html: '<strong>and easy to do anywhere, even with Node.js</strong>',
//     };

// sgMail.send(msg);